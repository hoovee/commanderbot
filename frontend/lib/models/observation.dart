import 'package:flutter/material.dart';
import 'package:frontend/common/enums.dart';

class ObservationModel {
  // create faction with HexColor.fromHex('#FF0000')
  final Color faction;
  final String source; // callsign
  final Type type;
  final String positionGrid;
  final int positionEasting;
  final int positionNorthing;
  int positionPixelHorizontal; // ??
  int positionPixelVertical; // ??

  final int movementDirection;
  final Size size;
  final int numOfVehicles;
  final String updated;

  ObservationModel(
      this.faction,
      this.source,
      this.type,
      this.positionGrid,
      this.positionEasting,
      this.positionNorthing,
      this.movementDirection,
      this.size,
      this.numOfVehicles,
      this.updated) {
    // todo make the calculations here
    positionPixelHorizontal = 0;
    positionPixelVertical = 0;
  }
}

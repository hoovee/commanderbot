import 'package:flutter/material.dart';
import 'package:frontend/common/enums.dart';
import 'package:frontend/common/map_calculations.dart';
import 'package:frontend/extensions/hex_color.dart';

class UnitModel {
  final String callsign;
  final Type type;
  final String lastPositionGrid;
  final int lastPositionEasting;
  final int lastPositionNorthing;
  final double positionPixelHorizontal;
  final double positionPixelVertical;
  // create faction with HexColor.fromHex('#FF0000')
  final Color faction;
  final String updated;
  final Color strength;
  final Color ammo;
  final Color water;
  final Color energy;
  final String status;

  UnitModel(
      this.callsign,
      this.type,
      this.lastPositionGrid,
      this.lastPositionEasting,
      this.lastPositionNorthing,
      this.positionPixelHorizontal,
      this.positionPixelVertical,
      this.faction,
      this.updated,
      this.strength,
      this.ammo,
      this.water,
      this.energy,
      this.status) {
    // todo make the calculations here
  }

  factory UnitModel.fromJson(Map<String, dynamic> json) {
    final double posPixelX = getXPixel(json["last_position_e"]);
    final double posPixelY = getYPixel(json["last_position_n"]);
    final Color color = HexColor.fromHex(json["partof"]["color"]);
    Color strength;
    if (json["strength"] == "G") {
      strength = Colors.green;
    } else if (json["strength"] == "Y") {
      strength = Colors.yellow;
    } else {
      strength = Colors.red;
    }
    Color ammo;
    if (json["ammo"] == "G") {
      ammo = Colors.green;
    } else if (json["ammo"] == "Y") {
      ammo = Colors.yellow;
    } else {
      ammo = Colors.red;
    }
    Color water;
    if (json["water"] == "G") {
      water = Colors.green;
    } else if (json["water"] == "Y") {
      water = Colors.yellow;
    } else {
      water = Colors.red;
    }
    Color energy;
    if (json["energy"] == "G") {
      energy = Colors.green;
    } else if (json["energy"] == "Y") {
      energy = Colors.yellow;
    } else {
      energy = Colors.red;
    }
    return UnitModel(
      json["callsign"],
      Type.inf,
      json["last_position_grid"],
      json["last_position_e"],
      json["last_position_n"],
      posPixelX,
      posPixelY,
      color,
      json["updated"],
      strength,
      ammo,
      water,
      energy,
      json["status"],
    );
  }
}

import 'package:flutter/material.dart';

class MsgLogPage extends StatefulWidget {
  MsgLogPage({Key key}) : super(key: key);

  @override
  _MsgLogPageState createState() => new _MsgLogPageState();
}

class _MsgLogPageState extends State<MsgLogPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Box Decoration and Stack example"),
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return new Text('Tutorial in progress');
  }
}

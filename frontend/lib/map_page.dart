import 'package:flutter/material.dart';
import 'msg_log_page.dart';
import 'dart:async';
import 'dart:ui';
import 'package:frontend/common/networking.dart';
import 'package:frontend/models/unit.dart';

class MapPage extends StatefulWidget {
  MapPage({Key key}) : super(key: key);

  @override
  _MapPageState createState() => new _MapPageState();
}

class _MapPageState extends State<MapPage> {
  @override
  Widget build(BuildContext context) {
    List<Widget> menu = <Widget>[
      new IconButton(
          icon: Icon(Icons.image),
          tooltip: 'Message Log page',
          onPressed: _toMsgLogPage),
    ];

    return new Scaffold(
      appBar: AppBar(
        title: Text("Map menu"),
        actions: menu,
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return CustomPaint(
      foregroundPainter: ShapePainter(),
      child: Container(
        constraints: BoxConstraints.expand(
          height: 1200.0,
        ),
        alignment: Alignment.bottomCenter,
        padding: new EdgeInsets.only(left: 16.0, bottom: 8.0),
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage('assets/TSTOS20MAP_fixed.jpg'),
            fit: BoxFit.contain,
          ),
        ),
      ),
    );
  }

  Future _toMsgLogPage() async {
    Navigator.of(context).push(new MaterialPageRoute<dynamic>(
      builder: (BuildContext context) {
        return new MsgLogPage();
      },
    ));
  }
}

class ShapePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) async {
    var paint = Paint()
      ..color = Colors.teal
      ..strokeWidth = 5
      ..style = PaintingStyle.fill
      ..strokeCap = StrokeCap.round;

    var _units = getUnits();
    _units.then((value) {
      for (var unit in value) {
        var path = Path();
        path.addOval(Rect.fromCircle(
          center:
              Offset(unit.positionPixelHorizontal, unit.positionPixelVertical),
          radius: 5,
        ));
        paint.color = unit.faction;
        canvas.drawPath(path, paint);
      }
    }, onError: (e) {
      print(e);
    });
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

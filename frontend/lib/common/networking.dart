import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:frontend/models/unit.dart';

Future<List<UnitModel>> getUnits() async {
  final _response = await http.get("http://localhost:8000/api/units/list/");
  final _units = <UnitModel>[];
  if (_response.statusCode == 200) {
    var _jsonResponse = convert.jsonDecode(_response.body);
    print("Obtained unit list of size $_jsonResponse['count']");
    for (var obj in _jsonResponse["results"]) {
      _units.add(UnitModel.fromJson(obj));
    }
  } else {
    print("Failed to fetch units!");
  }
  return _units;
}

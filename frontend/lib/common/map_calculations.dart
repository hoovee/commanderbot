// self.left_x = int(line[0])
// self.left_y = int(line[1])
// self.x_corr = int(line[2])
// self.y_corr = int(line[3])
// self.pixel_to_km = float(line[4])
// self.grid = line[5]
// x_pixel = abs(x - self.left_x) * self.pixel_to_km + self.x_corr
// y_pixel = abs(y - self.left_y) * self.pixel_to_km + self.y_corr

// x, y : MGRS values E and N
// left_x, left_y : MGRS value of top-left corner of map image (0px,0px)
// x_corr, y_corr : ??
// pixel_to_km = how many pixels in 1000m

// left_x = 06458
// left_y = 97285
// pixel_to_km = 1998 / 1000 = 1.998

import 'dart:core';

double getXPixel(int mgrsX) {
  double xM = (mgrsX - 6458).abs().toDouble();
  double x = xM * 1.998;
  return x;
}

double getYPixel(int mgrsY) {
  double yM = (mgrsY - 6458).abs().toDouble();
  double y = yM * 1.998;
  return y;
}

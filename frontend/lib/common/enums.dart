// single, patrol, squad, platoon, company, battalion
enum Size { one, ptr, sqd, plt, coy, bat }
// infantry, mechanized, recon, psyops, commander/hvt, building, offgame
enum Type { inf, mec, rec, pso, hvt, bld, off }

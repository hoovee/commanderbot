# CommanderBot

Telegram-based bot for running Tactical Operations Center (TOC) operations.

# Other documents

- commanderbot/README.md - Backend documentation
- commanderbot/REGISTRATION.md - Registration process
- commanderbot/ui/README.md - UI documentation

# Requirements

The bot uses [Python Telegram Bot](https://python-telegram-bot.readthedocs.io/en/latest/index.html) package and [Django](https://docs.djangoproject.com/en/3.0/).

For other requirements, see the Pipfile and [pipenv](https://pipenv-fork.readthedocs.io/en/latest/) docs.

# About logging

## Levels:
- INFO: Generic application info and how the application is functioning
- DEBUG: Messages that help developer to see what is happening
- WARNING: Bad user inputs, etc.
- ERROR: Weird situations, errors in database, etc.

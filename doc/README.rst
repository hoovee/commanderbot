===========================
CommanderBot documentations
===========================
Instructions how to generate documents for the project.

Setup PlantUML
--------------
1. Clone this `repo <git@github.com:plantuml/plantuml-server.git>`_.
2. Run PlantUML server with `docker-compose up --build`
3. Install PlantUML extension to VSCode `ext install jebbs.plantuml`. More details `here <https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml#user-content-use-plantuml-server-as-render>`_.

Running
-------
Run with `docker-compose up`.

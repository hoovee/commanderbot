# How to register to the system?

## What the user should do?

1. Search TieraCommandBot from the Telegram
2. Start the conversation with the bot by pressing the `/start` or `/restart` button
3. Press `/register` button
4. Type in your in-game callsign. Ask TOC for your callsign if you don't know it.
5. Notify TOC to check your registration (https://t.me/joinchat/Ax4piEkyZ5uddovRV3XdDQ) or ping directly @hoovee or @eeromatilainen in Telegram.
6. Click the button `Am I in yet?` when TOC has approved you to the system. The button can be clicked every now and then.

## What the TOC operator should do?

1. Check unapproved units in http://localhost:8000/api/units/list/unapproved/ when the bot system is running.
2. If there are units in the list, open the detailed page using the callsign on the URL: http://localhost:8000/api/units/list/unapproved/<callsign>
3. Verify in the OOB and the settings.py that the registered callsign is appropriate. **If the callsign does not qualify, then delete the object.** If it does, then continue.
4. On the "Approve Operational Unit" page, update the following information in the form
   1. Unit size and type
   2. Fix callsign if it has any typos
   3. Last position values:
      1. last_position_grid: 35VMH
      2. last_position_e: 7000
      3. last_position_n: 97000
   4. chat_id_to: Check from settings.py or Chats Excel sheet in Google Drive
   5. **Check the Approved** checkbox!
   6. Press the PUT button
5. Check http://localhost:8000/api/units/list/ that the new unit/user has appeared there.
6. Notify the user that he/she has been approved so he/she can click on the `Am I in yet?` button in the Telegram conversation.

## Explain these to user

1. Registration process
2. If server is reinitialized, you must register again but if the callsign is same, you are forwarded directly to the grid
   - write /start
   - use same callsign for registration
3. Avoid flooding the commanderbot chat
   - if you get error message, wait one minute and resend
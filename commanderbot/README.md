# CommanderBot

## How to setup on a new machine?

Install pipenv

    sudo -H python3 -m pip install pipenv

Open a terminal window, navigate to the root folder of this repository and start the pipenv virtual environment

    pipenv shell

Install required dependencies by typing

    pipenv install

Wait until the command finishes. Now the system should be ready to run.

## How to start?

Open a new terminal window, then go into commanderbot folder and run

    pipenv shell

This will open a new pip virtual environment which contains all the necessary system dependencies and libraries. After this navigate in the terminal to the ui folder

    cd commanderbot/

The folder contains `manage.py` which has to be run using python. Run it with command

    python manage.py runserver

The terminal will start displaying various log messages. If there is no `error` messages in the terminal you're good to go. The application will print various log messages into the terminal so if the system does not function properly check the terminal or log file for the errors.

The log files can be found under `log/` folder. Use some text editor to open them or grep them with command

    cat commanderbot.log | grep "error"

Another logfile is the `raw_msgs.log` which has log of every custom message sent to bot (which are not done by using the custom keyboard). You can also grep this log file with command

    cat raw_msgs.log | grep "error"

Finally you should open a web browser (Mozilla firefox for instance) and go to http://localhost:8000/api/ and there click the following link

    "start-bot": "http://localhost:8000/api/bot/start/"

This will start the bot. Starting the UI will also trigger this action so this is not necessary needed but in case you want to start only this application, remember to use the start-bot link.

## Stopping the application

On the terminal window you run the `python manage.py runserver`, press Ctrl + C few times to stop the application.

## Important info

The commanderbot application is based on Django which is a Python library for building webservices.

Another library used for the application is python-telegram-bot library which handles the Telegram integration.

### Important files

Note that the locations here start from the root of the **repository** not in your computer's filesystem.

- `commanderbot/db.sqlite3`: Django database file (made with SQLite).
- `commanderbot/commanderbot_persistence_info`: python-telegram-bot database file (made with Pickle).
- `commanderbot/manage.py`: Script to start and manage the Django application.
- `commanderbot/README.md`: This file. Very important info!
- `commanderbot/commanderbot/settings.py`: Settings file for the Django application. See next section for details.

### Application settings

Note that the locations here start from the root of the **repository** not in your computer's filesystem.

Location: `commanderbot/commanderbot/settings.py`.

The commanderbot application's main configurations are in this file. When running the system during the game, the variable `PRODUCTION_MODE_ON` should be set to `True` in order not to exceed Telegram bot API limitations. All other important settings are located under the `BOT_CONFIG` dictionary:

- `API-TOKEN`: Telegram bot API token. The bot owner needs to rewoke this.
- `PERSISTENCE_FILE`: Telegram bot database file path.
- `OWN-FACTION-NAME`: Own faction name in-game.
- `ENEMY-FACTION-NAME`: Enemy faction name in-game.
- `CIVILIAN-FACTION-NAME`: Civilian faction name in-game.
- `YELLOW-FACTION-NAME`: 3rd party faction name in-game.
- `POSSIBLE_CALLSIGNS`: Dictionary of callsigns and chat ids that are used to talk to the user through the Telegram bot.

## API description (what to find and where)

- http://localhost:8000/api/factions/ : Faction API page.
  - http://localhost:8000/api/factions/list/ : List of factions in the database.
  - http://localhost:8000/api/factions/<name>/ : Show details of a faction. Parameter <name> needs to be replaces with the faction name in the browsers URL field. For example http://localhost:8000/api/factions/Pirkanmaa/ would show details of the Pirkanmaa database object.
    - http://localhost:8000/api/factions/<name>/observed/ : List of faction units observed by own faction units. These are the observation results made by the operational units. Parameter <name> needs to be replaced with the faction name in the browser's URL field. For example http://localhost:8000/api/factions/Pirkanmaa/observed/ would show Pirkanmaa units that have been observed.
- http://localhost:8000/api/units/ : Units API page.
  - http://localhost:8000/api/units/list/ : List of approved units in-game.
  - http://localhost:8000/api/units/<callsign>/ : Details of approved units in-game. The unit here must be deleted in order to register the callsign to some other Telegram user.
    - http://localhost:8000/api/units/<callsign>/locations/ : Locations the unit has shared to the bot.
    - http://localhost:8000/api/units/<callsign>/observations/ : Observations the unit has put to the bot.
  - http://localhost:8000/api/units/list/unapproved/ : List of unapproved units in-game. These need to be processed by TOC operators individually each.
    - http://localhost:8000/api/units/list/unapproved/<callsign>/ : Shows the detailed information of the unapproved unit. From this page the TOC operators can edit the information of the unit or delete the unit. Deletion will then reject the user's registration. **Note**: Each Telegram user has an unique id which cannot be same for two different operational units. In addtion, there cannot be two same callsigns in the database.
- http://localhost:8000/api/bot/start/ : Start the bot.
- http://localhost:8000/api/bot/stop/ : Stop the bot (not really needed).

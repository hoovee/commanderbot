"""File containing class ControlPanel which is docked to main window."""

import datetime
import logging
import threading
import time
import os

from dateutil.parser import parse as dateparse

from PyQt5.QtCore import Qt, pyqtSlot
from PyQt5.QtGui import QColor, QIntValidator
from PyQt5.QtWidgets import (QCheckBox, QGridLayout, QLabel, QPushButton,
                             QTableWidget, QTableWidgetItem, QLineEdit,
                             QWidget, QVBoxLayout, QHBoxLayout)

from lib.api import APIClient
from config import BASE_URL, UPDATE_FREQ, OBSERVATION_EXPIRATION_TIME

logger_cpw = logging.getLogger('commanderbot-ui.control-panel')


class UpdatesThread(threading.Thread):
    """Thread for getting updates from TG API.

    This thread gets continuously updates from Telegram API.
    It inherits the threading Thread class.

    Args:
        threading (Thread): Thread class of threading module.
    """

    def __init__(self, threadID, name, parent):
        """Initialize UpdatesThread.

        This function initializes the updates thread

        Args:
            threadID (int): thread id
            name (str): thread name
            parent (QWidget): ControlPanel class.
        """
        threading.Thread.__init__(self)
        self.id = threadID
        self.name = name
        self.parent = parent
        self._stop_event = threading.Event()

    def run(self):
        """Run thread."""
        logger_cpw.info("Starting updates thread")
        self.parent.update_locations()
        logger_cpw.info("Exiting updates thread")

    def stop(self):
        """Stop thread."""
        self._stop_event.set()


class ControlPanel(QWidget):
    """ControlPanel class implements the control panel.

    The control panel uses the telegram bot commands to send messages,
    etc.
    """

    def __init__(self, parent):
        """Initialize control panel window."""
        super(ControlPanel, self).__init__(parent)
        logger_cpw.info("Creating Control Panel")
        self.parent = parent
        # initialize UI
        self.init_ui()
        # initialize bot
        self.api = APIClient()
        if not self.api.start_bot(f"{BASE_URL}/api/bot/start/"):
            print("Failed to connect bot. Please exit program with Ctrl + C")
            return
        # init units data
        self.units_data = {}
        # observation expiration time
        self.expiration_time = OBSERVATION_EXPIRATION_TIME
        # create unit fetcher thread
        thread_name = "unit_fetcher"
        self.unitsThread = UpdatesThread(1, thread_name, self)
        self.stop_thread = False
        self.unitsThread.start()

    def init_ui(self):
        """Initialize ui."""
        # Tables of locations
        # INF locations
        locations_label = QLabel("Unit locations")
        self.locations_table = QTableWidget()
        self.locations_table.setColumnCount(7)
        self.locations_table.setRowCount(18)

        self.locations_table.setHorizontalHeaderLabels(
            ["Location", "Timestamp", "Strength", "Ammo", "Water", "Energy", "Status"]
        )

        for row in range(self.locations_table.rowCount()):
            for col in range(self.locations_table.columnCount()):
                self.locations_table.setItem(row, col, QTableWidgetItem())

        # Create layout
        main_grid = QVBoxLayout()

        locations_grid = QHBoxLayout()
        loc_grid = QVBoxLayout()

        # signal to drawing
        self.draw_loc_button = QPushButton("Draw map")
        self.draw_loc_button.clicked.connect(self.parent.map_window.draw_units)

        expiration_field_info = QLabel("Filter observations by minutes (default 30)")
        self.observation_expiration_input = QLineEdit(f"{OBSERVATION_EXPIRATION_TIME}", self)
        self.observation_expiration_input.setValidator(QIntValidator(1, 10000))
        self.observation_expiration_input.setMaxLength(5)
        self.observation_expiration_input.setAlignment(Qt.AlignLeft)
        self.observation_expiration_input.textChanged.connect(self.expiration_updated)

        settings_layout = QHBoxLayout()
        settings_grid = QVBoxLayout()
        settings_grid.addWidget(expiration_field_info)
        settings_grid.addWidget(self.observation_expiration_input)

        settings_layout.addLayout(settings_grid)
        settings_layout.addWidget(self.draw_loc_button)

        loc_grid.addLayout(settings_layout)

        info_text = QLabel(
            "Unit status:\n"
            "ASM = About to start a mission\n" +
            "CWM = Continuing with the mission\n" +
            "RTB = Returning to base\n" +
            "WFM = Waiting for a mission"
        )
        loc_grid.addWidget(info_text)
        loc_grid.addWidget(locations_label)
        loc_grid.addWidget(self.locations_table)

        locations_grid.addLayout(loc_grid)
        # add locations to main grid
        main_grid.addLayout(locations_grid)
        self.setLayout(main_grid)

    def expiration_updated(self, value):
        if value:
            self.expiration_time = value

    def get_expiration_time(self):
        return self.expiration_time

    def get_locations(self):
        return self.units_data

    def kill_thread(self):
        """Stop locations thread."""
        logger_cpw.info("Stopping updates thread")
        self.stop_thread = True

    def select_color(self, text):
        """Convert text R, G, Y to according colors.

        R = Red, G = Green, Y = Yellow.
        """
        if text == "G":
            return QColor("green")
        elif text == "Y":
            return QColor("yellow")
        else:
            return QColor("red")

    def update_loc_table(self, unit, timestamp, mgrs, strength, ammo, water, energy, status):
        """Update locations in tables.

        Updates unit's location and timestamp. Utilizes the verticalHeader
        items of tables.

        Args:
            unit (str): Unit name.
            timestamp (str): Timestamp.
            mgrs (str): String representation of mgrs coordinates of unit.
            strength (str): Strength in either R,G,Y.
            ammo (str): Ammo in either R,G,Y.
            water (str): Water in either R,G,Y.
            energy (str): Energy in either R,G,Y.
            status (str): Status string.
        """
        found = False
        # try finding unit already from the table
        for i in range(0, self.locations_table.rowCount()):
            if (self.locations_table.verticalHeaderItem(i) is not None and
                    self.locations_table.verticalHeaderItem(i).text() == unit):
                found = True
                self.locations_table.item(i, 0).setText(mgrs)
                self.locations_table.item(i, 1).setText(timestamp)
                self.locations_table.item(i, 2).setBackground(self.select_color(strength))
                self.locations_table.item(i, 3).setBackground(self.select_color(ammo))
                self.locations_table.item(i, 4).setBackground(self.select_color(water))
                self.locations_table.item(i, 5).setBackground(self.select_color(energy))
                self.locations_table.item(i, 6).setText(status)
        # unit was not on the table so append it to next free slot
        if not found:
            for i in range(0, self.locations_table.rowCount()):
                if self.locations_table.verticalHeaderItem(i) is None:
                    self.locations_table.setVerticalHeaderItem(i, QTableWidgetItem(unit))
                    self.locations_table.item(i, 0).setText(mgrs)
                    self.locations_table.item(i, 1).setText(timestamp)
                    self.locations_table.item(i, 2).setBackground(self.select_color(strength))
                    self.locations_table.item(i, 3).setBackground(self.select_color(ammo))
                    self.locations_table.item(i, 4).setBackground(self.select_color(water))
                    self.locations_table.item(i, 5).setBackground(self.select_color(energy))
                    self.locations_table.item(i, 6).setText(status)
                    return

    def update_locations(self):
        """Update locations in locations.

        Get updates from TG Bot API and parse locations from
        the updates response.
        """
        while not self.stop_thread:
            response = self.api.get(f"{BASE_URL}/api/units/list/")
            for unit_obj in response:
                callsign = unit_obj["callsign"]
                self.units_data[callsign] = {}
                self.units_data[callsign]["info"] = unit_obj
                # now fetch all observations for this unit
                self.units_data[callsign]["observations"] = []
                minutes = OBSERVATION_EXPIRATION_TIME
                minutes = self.get_expiration_time()
                obs_resp = self.api.get(
                    f"{BASE_URL}/api/units/{callsign}/observations/{minutes}/",
                    ["ordering=-created"]
                )
                for observation in obs_resp:
                    self.units_data[callsign]["observations"].append(observation)
                grid = self.units_data[callsign]["info"]["last_position_grid"]
                easting = self.units_data[callsign]["info"]["last_position_e"]
                northing = self.units_data[callsign]["info"]["last_position_n"]
                mgrs = f"{grid} {easting} {northing}"
                updated = dateparse(self.units_data[callsign]["info"]["updated"])
                timestamp = datetime.datetime.fromtimestamp(updated.timestamp()).strftime("%H:%M:%S %d-%m-%Y")
                self.update_loc_table(callsign, timestamp, mgrs,
                                      self.units_data[callsign]["info"]["strength"],
                                      self.units_data[callsign]["info"]["ammo"],
                                      self.units_data[callsign]["info"]["water"],
                                      self.units_data[callsign]["info"]["energy"],
                                      self.units_data[callsign]["info"]["status"])
            self.draw_loc_button.click()
            # sleep x s before fetching again
            time.sleep(UPDATE_FREQ)
        self.unitsThread.stop()

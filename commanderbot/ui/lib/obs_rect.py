from PyQt5.QtWidgets import (QGraphicsTextItem, QGraphicsRectItem)


class ObsRectItem(QGraphicsRectItem):
    """Class for displaying single observation on a map.

    Inherits QGraphicsRectItem. It also creates QGraphicsTextItem next to it
    to display the unit's name.

    Args:
        QGraphicsRectItem (QGraphicsRectItem): Inherited QGraphicsRectItem.
    """

    def __init__(self, pos_x, pos_y, width, height, parent=None):
        """Initialize ObsRectItem.

        Sets initial position for the unit, creates the QGraphicsTextItem
        and hides it and sets the color for the unit using QBrush.

        Args:
            pos_x (float): X coordinate of the unit on the map coordinates.
            pos_y (float): Y coordinate of the unit on the map coordinates.
            width (float): Width of the rectangle.
            height (float): Height of the rectangle.
        """
        super(ObsRectItem, self).__init__(pos_x, pos_y, width, height, parent)
        self.show_text = False
        self.text = None
        self.timestamp = None
        self.id = None
        self.pos_x = self.pos().x()
        self.pos_y = self.pos().x()
        self.measures = (width, height)
        self.setRect(pos_x, pos_y, width, height)

    def create_text(self, unit_name):
        """Set text for the text item showing the unit's name.

        Create QGraphicsTextItem and hide it.

        Args:
            unit_name (str): Unit's name.
        """
        self.text = QGraphicsTextItem(parent=self)
        self.text.setPlainText(unit_name)
        self.text.setVisible(self.show_text)
        self.set_text_position()

    def set_text_position(self):
        """Set text position based on the position of the unit rect item.

        Uses QGraphicsItem's setPos function.
        Text position is defined by:
            x: item.pos_x + width/2 + 2
            y: item.pos_y - height
        """
        x = self.measures[0]/2.0 + 2
        y = - self.measures[1]
        self.pos_x = self.pos().x()
        self.pos_y = self.pos().y()
        self.text.setPos(x, y)

    def make_text_visible(self):
        """Set unit name visible."""
        self.text.setVisible(True)

    def make_text_hidden(self):
        """Set unit name hidden."""
        self.text.setVisible(False)

    def mousePressEvent(self, event):
        """Override for mouse press event.

        When mouse is pressed on top the the unit instance, it either
        shows or hides the unit's name text object.

        Args:
            event (QGraphicsSceneMouseEvent): Click event.
        """
        self.show_text = not self.show_text
        self.text.setVisible(self.show_text)

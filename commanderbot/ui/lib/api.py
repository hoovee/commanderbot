"""API functions for getting data from commanderbot application."""

import logging
import requests

logger_api = logging.getLogger('commanderbot-ui.api')


class APIClient:
    """Class for handling communication to CommanderBot API."""

    def __init__(self):
        super().__init__()

    def start_bot(self, url):
        response = requests.get(url)
        if response.status_code == 200:
            return True
        else:
            logger_api.error(
                f"Failed to connect server on url {url}. Response status code was {response.status_code}.")
            return False

    def get(self, url, filters=None):
        """Get data in JSON format from given URL.

        Args:
            url (string): URL to fetch data from.
            filters (list): List of filters that can be applied to query.
                            They are appended as &filter1&filter2.
                            Defaults to None.

        Returns:
            Dict{string:string}: JSON serializable object.
        """
        get_url = f"{url}?format=json"
        if filters is not None:
            for f in filters:
                get_url = f"{get_url}&{f}"
            logger_api.debug(f"Final request url with filters is {url}.")
        response = requests.get(get_url)
        if response.status_code == 200:
            return response.json()
        else:
            logger_api.warning(
                f"Failed to get data from URL: {get_url}. Response status code was {response.status_code}.")
            return None

    def post(self, url, obj):
        """Post given object to given URL.

        Args:
            url (string): URL to post data to.
            obj (Dict{string:string}): JSON serializable object.

        Returns:
            bool: True if response status code was 201. False otherwise.
        """
        response = requests.post(url, data=obj)
        if response.status_code == 201:
            return True
        else:
            logger_api.warning(f"Failed to post data to URL: {url}. Response status code was {response.status_code}.")
            return False

    def put(self, url, obj):
        response = requests.put(url, data=obj)
        if response.status_code == 200:
            return True
        else:
            logger_api.warning(f"Failed to put data to URL: {url}. Response status code was {response.status_code}.")
            return False

import logging
import threading


logger_drawing = logging.getLogger('commanderbot-ui.map-window.drawing-thread')


class DrawingThread(threading.Thread):
    """Thread for drawing units to the map.

    This thread fetches continuously the locations from
    control panel class and draws units based on those.

    Args:
        threading (Thread): Thread class of threading module.
    """

    def __init__(self, threadID, name, parent):
        """Initialize DrawingThread.

        This function initializes the DrawingThread.

        Args:
            threadID (int): Thread id.
            name (str): Thread name.
            parent (QWidget): MapWindow class.
        """
        threading.Thread.__init__(self)
        self.id = threadID
        self.name = name
        self.parent = parent
        self._stop_event = threading.Event()

    def run(self):
        """Run thread."""
        logger_drawing.info("Starting drawing thread")
        self.parent.draw_units()

    def stop(self):
        """Stop thread."""
        self._stop_event.set()

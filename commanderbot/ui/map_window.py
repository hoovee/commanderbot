"""File containing class map window which displays the map."""

import csv
import time
import logging
import datetime
from dateutil.parser import parse as dateparse

from PyQt5.QtCore import Qt, pyqtSlot
from PyQt5.QtGui import QBrush, QPen, QPixmap
from PyQt5.QtWidgets import (QGraphicsScene, QGraphicsView, QVBoxLayout,
                             QWidget, QGraphicsPixmapItem,
                             QGraphicsTextItem, QCheckBox)

from lib.unit_rect import UnitRectItem
from lib.obs_rect import ObsRectItem
from lib.api import APIClient
from lib.drawing import DrawingThread
from config import MAP_FILE_PATH
from config import OBSERVATION_EXPIRATION_TIME


logger_map = logging.getLogger('commanderbot.map-window')

# TODO: Add click event to map (clicking prints MGRS coordinate)


class MapWindow(QWidget):
    """Class widget for map window.

    Display map and update items on it.
    """

    def __init__(self, parent):
        """Initialize map window."""
        super(MapWindow, self).__init__(parent)
        logger_map.info("Creating Map Window")
        self.parent = parent
        self.mapfile = (MAP_FILE_PATH)
        # dict for storing unit items based on unit name
        self.unit_items = {}
        # tuple containing map boundaries
        self.mapsize = ()
        # dict for observations
        self.observations = {}
        # QGraphicsPixmapItem object variable
        self.pixmap = None
        # QGraphicsScene object variable
        self.scene = None
        self.is_checked = False
        # initialize values for map corrections
        self.left_x = 0
        self.left_y = 0
        self.x_corr = 0
        self.y_corr = 0
        self.pixel_to_km = 0
        self.scale_value = 1
        self.grid = ""
        self.get_map_configuration('./map_files/TSTOS20/map_config.csv')
        # initialize UI
        self.init_ui()
        self.api = APIClient()

    def init_ui(self):
        """Initialize ui."""
        self.scene = QGraphicsScene(self)
        # setup the map as QPixmap
        pixmap = QPixmap(self.mapfile)
        # scale by ignoring the aspect ratio and using smooth transform mode
        scaled_pixmap = pixmap.scaled(pixmap.width() * self.scale_value, pixmap.height() * self.scale_value, 0, 1)
        self.pixmap = QGraphicsPixmapItem(scaled_pixmap)
        self.mapsize = (scaled_pixmap.width(), scaled_pixmap.height())

        self.scene.addItem(self.pixmap)
        self.scene.setSceneRect(0, 0, self.mapsize[0], self.mapsize[1])
        # setup graphicsview
        self.view = QGraphicsView(self.scene, self)
        # checkbox for showing all texts next to units
        self.all_texts_cb = QCheckBox("Show texts", self)
        self.all_texts_cb.toggle()
        self.all_texts_cb.setChecked(False)
        self.all_texts_cb.stateChanged.connect(self.make_all_texts_visible)
        # setup layout
        layout = QVBoxLayout()

        layout.addWidget(self.view)
        layout.addWidget(self.all_texts_cb)

        self.setLayout(layout)

    def get_map_configuration(self, conffile):
        """Get map configuration.

        Read map configuration from given csv file path.

        Args:
            conffile (str): Path to configuration file.
        """
        first = True
        with open(conffile, 'r') as confcsv:
            csvdata = csv.reader(confcsv, delimiter=",")
            for line in csvdata:
                if first:
                    first = False
                else:
                    self.left_x = int(line[0])
                    self.left_y = int(line[1])
                    self.x_corr = int(line[2])
                    self.y_corr = int(line[3])
                    self.pixel_to_km = float(line[4])
                    self.scale_value = float(line[5])
                    self.grid = line[6]

    @pyqtSlot()
    def make_all_texts_visible(self):
        """Hide or show unit names.

        If checkbox all_texts is checked, make all texts visible,
        otherwise hide them.
        """
        sender = self.sender()
        if sender.isChecked():
            self.is_checked = True
            for item in self.unit_items.keys():
                self.unit_items[item].make_text_visible()
            for item2 in self.observations.keys():
                self.observations[item2].make_text_visible()
        else:
            self.is_checked = False
            for item in self.unit_items.keys():
                self.unit_items[item].make_text_hidden()
            for item2 in self.observations.keys():
                self.observations[item2].make_text_hidden()

    def get_locations(self):
        return self.parent.control_panel.get_locations()

    def calculate_pixel_values(self, grid, easting, norting):
        """Calculate pixel values for mgrs coordinate given.

        Formula: x_pixel = abs(x_mgrs - upleft_x) * pixel/km + x_correction

        Args:
            mgrs (str): MGRS coordinates.

        Returns:
            tuple (int, int): Tuple containing pixel coordinates for x and y.

        """
        grid = grid
        x = easting
        y = norting
        x_pixel = -1
        y_pixel = -1
        # check if we are on the same grid
        if grid == self.grid:
            x_pixel = abs(x - self.left_x) * self.pixel_to_km * self.scale_value + self.x_corr
            y_pixel = abs(y - self.left_y) * self.pixel_to_km * self.scale_value + self.y_corr
        else:
            msg = f"Wrong grid provided: {grid}"
            logger_map.warning(msg)
        return (x_pixel, y_pixel)

    def not_outside_boundaries(self, point):
        """Check if coordinate point is outside of boundaries.

        Compare point x and y to size of the pixmap.

        Args:
            point (tuple(float, float)): Tuple containing x and y coordinate.

        Returns:
            bool: If outside boundaries return False, otherwise return True.

        """
        x = point[0]
        y = point[1]
        if x < 0 or x > self.mapsize[0]:
            return False
        if y < 0 or y > self.mapsize[1]:
            return False
        return True

    def obs_too_old(self, obs_rect):
        timezone = obs_rect.timestamp.tzinfo
        now = datetime.datetime.now(timezone)
        # print(f"Now: {now}")
        min_value = now - datetime.timedelta(minutes=self.parent.control_panel.get_expiration_time())
        res = (obs_rect.timestamp < min_value)
        # print(f"Comparing {obs_rect.timestamp} and {min_value}, result was {res}")
        return res

    @pyqtSlot()
    def draw_units(self):
        """Draw units to map.

        Used by drawingThread.
        """
        # clear ObsRectItems
        for key, value in self.observations.items():
            self.scene.removeItem(value)
        self.observations = {}
        # create items to map based on initial locations
        # and save each rect position under unit : QRect
        pen = QPen(Qt.black)
        units = self.get_locations()
        for unit in units.keys():
            grid = units[unit]["info"]["last_position_grid"]
            e = units[unit]["info"]["last_position_e"]
            n = units[unit]["info"]["last_position_n"]
            point = self.calculate_pixel_values(grid, e, n)
            if self.not_outside_boundaries(point):
                if unit in self.unit_items.keys():
                    self.unit_items[unit].setPos(point[0]-5, point[1]-5)
                else:
                    brush = QBrush(Qt.blue)
                    unit_rect = UnitRectItem(0, 0, 10, 10)
                    unit_rect.create_text(unit)
                    unit_rect.setBrush(brush)
                    unit_rect.setToolTip(unit)
                    unit_rect.setPen(pen)
                    self.scene.addItem(unit_rect)
                    unit_rect.setPos(point[0]-5, point[1]-5)
                    self.unit_items[unit] = unit_rect
                for obs in units[unit]["observations"]:
                    obs_grid = obs["position_grid"]
                    obs_e = obs["position_e"]
                    obs_n = obs["position_n"]
                    obs_point = self.calculate_pixel_values(obs_grid, obs_e, obs_n)
                    if self.not_outside_boundaries(obs_point):
                        obs_id = obs["id"]
                        brush = None
                        faction = obs["faction"]["name"]
                        if faction == "UN":
                            brush = QBrush(Qt.yellow)
                        elif faction == "Civilians":
                            brush = QBrush(Qt.green)
                        elif faction == "Pirkanmaa":
                            brush = QBrush(Qt.red)
                        else:
                            brush = QBrush(Qt.red)
                        unit_type = obs["type"]
                        unit_size = obs["size"]
                        vehicles = obs["num_of_vehicles"]
                        movement_dir = obs["movement_direction"]
                        source = obs["source"]["callsign"]
                        obs_rect = ObsRectItem(0, 0, 10, 10)
                        obs_rect.id = obs_id
                        obs_rect.timestamp = dateparse(obs["updated"])
                        obs_rect.create_text(f"{unit_type},{unit_size}\n{vehicles},{movement_dir}")
                        obs_rect.setBrush(brush)
                        obs_rect.setToolTip(f"Reported by: {source}")
                        obs_rect.setPen(pen)
                        self.scene.addItem(obs_rect)
                        obs_rect.setPos(obs_point[0]-5, obs_point[1]-5)
                        if self.is_checked:
                            obs_rect.make_text_visible()
                        self.observations[obs_id] = obs_rect
            else:
                msg = f"Unit {unit} outside of the map {grid} {e} {n} / {point[0]}, {point[1]}"
                logger_map.warning(msg)

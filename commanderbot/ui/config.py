"""Configuration variables."""

BASE_URL = "http://localhost:8000"

MAP_FILE_PATH = './map_files/TSTOS20/TSTOS20MAP_fixed.jpg'

# update frequency in seconds
UPDATE_FREQ = 5

# minutes
OBSERVATION_EXPIRATION_TIME = 30

"""Main window of the commanderbot system."""

import logging
import sys

from PyQt5.QtCore import QRegExp, pyqtSlot
from PyQt5.QtGui import QRegExpValidator
from PyQt5.QtWidgets import (QAction, QApplication, QLabel, QLineEdit,
                             QMainWindow, QPushButton, QVBoxLayout, QWidget)

from main_window import MainWindow


# set up logging to file - see previous section for more details
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
    datefmt='%d-%m-%Y %H:%M',
    filename='./log/commanderbot_ui.log',
    filemode='a'
)
# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(logging.INFO)
# set a format which is simpler for console use
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
# tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)


logger_main = logging.getLogger('commanderbot-ui.main-window')


class CommanderBotApp(QMainWindow):
    """Main application window for CommanderBot."""

    def __init__(self, parent=None):
        """Initialize application."""
        super(CommanderBotApp, self).__init__(parent)
        self.title = "CommanderBot Reporting system"
        self.left = 10
        self.top = 10
        self.width = 1024
        self.height = 600
        self.main_window = None
        self.id_window = None
        logger_main.info("Create main window instance")
        self.init_ui()

    def init_ui(self):
        """Initialize user-interface."""
        self.main_window = MainWindow(self)

        # Menubar
        menubar = self.menuBar()
        mainMenu = menubar.addMenu('&Main')
        exitButton = QAction('Exit', self)
        exitButton.triggered.connect(self.closeEvent)
        mainMenu.addAction(exitButton)

        # Set title, geometry and show all
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.showMaximized()

        self.setCentralWidget(self.main_window)

    def closeEvent(self, event):
        """Close event of the application."""
        logger_main.info("Closing application")
        self.main_window.control_panel.kill_thread()
        event.accept()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = CommanderBotApp()
    sys.exit(app.exec_())

# CommanderBot UI

## System setup

Install Qt on the computer running this software by typing in a terminal window

    sudo apt-get install qt5-default

Qt is the software used for creating the user interface.

Next install pipenv

    sudo -H python3 -m pip install pipenv

Open a terminal window, navigate to the root folder of this repository and start the pipenv virtual environment

    pipenv shell

Install required dependencies by typing

    pipenv install

Wait until the command finishes. Now the system should be ready to run.

## How to start?

Open a new terminal window, then go into commanderbot folder and run

    pipenv shell

This will open a new pip virtual environment which contains all the necessary system dependencies and libraries. After this navigate in the terminal to the ui folder

    cd commanderbot/ui/

The folder contains `main.py` which has to be run using python. Run it with command

    python main.py

The terminal will start displaying various log messages. If there is no `error` messages in the terminal you're good to go. After a while the program opens two windows: Control panel window which shows table of unit information and a Map window which shows the map. To exit the program, click Exit on Main menu located in the top-left corner in Control panel window. You can also exit the program by individually closing the windows starting from the Map window.

## Configuring the system

If the map is too big for your screen, open a file explorer and navigate to the `commanderbot/commanderbot/ui/map_files/TSTOS20/` folder. In that folder, there exists `map_config.csv` which has following info in it:

```
left_x,left_y,x_corr,y_corr,pixel_to_km,default_scale_value,grid
6458,97285,0,0,1.998,0.2,35VMH
```

Try to put the `default_scale_value` smaller to decrease the size of the map. **DO NOT TOUCH ANY OTHER PARAMETERS!**
For FullHD screen, probably a good value is `0.12` but you can experiment with different values to find the optimal for you.

## Other info

Folder `log/` contains all the log files for the ui. Open the file with some IDE or text editor or grep it for errors with command

    cat commanderbot_ui.log | grep "error"

File `config.py` holds other configuration variables such as path to wanted map file and base URL for making API requests. Change these configurations accordingly based on your setup.

"""Main window of the application."""
from PyQt5.QtWidgets import (QHBoxLayout, QWidget)

from control_panel_window import ControlPanel
from map_window import MapWindow


class MapWidget(QWidget):
    """New window to show map window."""

    def __init__(self, parent, map_window):
        """Initialize the NewIDwidget.

        Create the NewIDwidget.

        Args:
            parent (QMainWindow): The commanderbot main window.
        """
        super().__init__()
        self.parent = parent
        self.map_win = map_window
        self.init_ui()

    def init_ui(self):
        """Initialize ui."""
        layout = QHBoxLayout()
        layout.addWidget(self.map_win)
        self.setLayout(layout)

        self.setGeometry(10, 10, self.map_win.mapsize[0], self.map_win.mapsize[1])
        self.setWindowTitle('Map window')


class MainWindow(QWidget):
    """Main central widget for the CommanderBot application.

    This initializes other windows.
    """

    def __init__(self, parent):
        """Initialize main window."""
        super(MainWindow, self).__init__(parent)

        self.map_window = None
        self.control_panel = None
        self.layout = None
        self.init_ui()

    def init_ui(self):
        """Initialize all parts of the main window."""
        self.layout = QHBoxLayout()

        self.map_window = MapWindow(self)
        self.control_panel = ControlPanel(self)

        self.map_showing_window = MapWidget(self, self.map_window)
        self.map_showing_window.show()
        # self.layout.addWidget(self.map_window)
        self.layout.addWidget(self.control_panel)

        self.setLayout(self.layout)

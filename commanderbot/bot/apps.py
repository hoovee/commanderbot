import logging
from django.apps import AppConfig

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
    datefmt='%d-%m-%Y %H:%M',
    filename='./log/commanderbot.log',
    filemode='a'
)
# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
# set a format which is simpler for console use
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
# tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)


class BotConfig(AppConfig):
    name = 'bot'

    def ready(self):
        from bot.telegram.commanderbot import CommanderBot
        from commanderbot.settings import BOT_CONFIG
        self.bot = CommanderBot(BOT_CONFIG['API-TOKEN'])

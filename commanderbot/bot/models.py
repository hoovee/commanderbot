from django.db import models
from django.utils.timezone import now
from bot.constants import unit_types as ut
from bot.constants import unit_status as us


class Faction(models.Model):
    """Model representing a faction in the game.

    Fields:
        - name : Name of the faction
        - color : Color as hex value
        - description : Detailed description of the faction

    """
    name = models.CharField(max_length=200, unique=True)
    color = models.CharField(max_length=7, default="#FFFFFF")
    description = models.CharField(max_length=500)


class Unit(models.Model):
    """Model representing single unit that belongs to some faction.

    Fields:
        - callsign : Name of the unit
        - description : Detailed description of the unit
        - size : Size of the unit, using unit_types.py
        - type : Type of the unit, using unit_types.py
        - partof : To which faction the unit belongs to
        - last_position_grid : MGRS grid
        - last_position_e : Easting of the last position
        - last_position_n : Northing of the last position
        - created : Timestamp when the unit was created into the system
        - updated: Timestamp when the unit was last updated

    """
    callsign = models.CharField(max_length=200, unique=True)
    description = models.CharField(max_length=500)
    size = models.CharField(max_length=3, choices=ut.UNIT_SIZES, default=ut.PATROL)
    type = models.CharField(max_length=3, choices=ut.UNIT_TYPES, default=ut.INFANTRY)
    last_position_grid = models.CharField(default='35VLG', max_length=5)
    last_position_e = models.IntegerField(default=0)
    last_position_n = models.IntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class OperationalUnit(Unit):
    """Model representing operational unit that has some chat.

    The chat refers to Telegram private chat that the actual user and the bot
    communicate in.

    Args:
        Unit (Model): This class inherits the Unit model.

    Fields:
        - chat_id_from: Telegram API chat id that messages are received
                        (Private chat with the user).
        - chat_id_to: Telegram API chat id that messages are relayed to
                      (TOC channel).
    """
    chat_id_from = models.IntegerField(default=-1, unique=True)
    chat_id_to = models.IntegerField(default=-1)
    approved = models.BooleanField(default=False)
    partof = models.ForeignKey('Faction', on_delete=models.CASCADE, related_name='units')
    strength = models.CharField(max_length=3, choices=us.STATUS_QUANTITIES, default=us.GREEN)
    ammo = models.CharField(max_length=3, choices=us.STATUS_QUANTITIES, default=us.GREEN)
    water = models.CharField(max_length=3, choices=us.STATUS_QUANTITIES, default=us.GREEN)
    energy = models.CharField(max_length=3, choices=us.STATUS_QUANTITIES, default=us.GREEN)
    status = models.CharField(max_length=3, choices=us.MISSION_STATUS, default=us.MISSION_STATUS_WAITING_FOR_MISSION)


class Location(models.Model):
    """Model representing location shared via Telegram message.

    Args:
        Message (Model): This class inherits the Message model.

    Fields:
        - latitude: Latitude of the shared location.
        - longitude: Longitude of the shared location.
        - mgrs_grid: MGRS coordinate system grid.
        - mgrs_e: MGRS coordinate Easting.
        - mgrs_n: MGRS coordinate Northing.

    """
    source = models.ForeignKey('OperationalUnit', on_delete=models.CASCADE, related_name='locations')
    latitude = models.DecimalField(default=-1.0, max_digits=10, decimal_places=7)
    longitude = models.DecimalField(default=-1.0, max_digits=10, decimal_places=7)
    mgrs_grid = models.CharField(default='35VLG', max_length=5)
    mgrs_e = models.IntegerField(default=0)
    mgrs_n = models.IntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)


class Observation(models.Model):
    """Model representing observation made by OperationalUnit.

    This model uses the Message model as a base. This kind of model instance is
    created based on the observation of OperationalUnit instance and these
    can be then displayed differently to the users than the OperationalUnits.
    """
    faction = models.ForeignKey('Faction', on_delete=models.CASCADE, related_name='detected_units')
    source = models.ForeignKey('OperationalUnit', on_delete=models.CASCADE, related_name='observer')
    type = models.CharField(max_length=3, choices=ut.UNIT_TYPES, default=ut.INFANTRY)
    position_grid = models.CharField(default='35VLG', max_length=5)
    position_e = models.IntegerField(default=0)
    position_n = models.IntegerField(default=0)
    movement_direction = models.IntegerField(default=0)
    size = models.CharField(max_length=3, choices=ut.UNIT_SIZES, default=ut.PATROL)
    num_of_vehicles = models.CharField(default="0", max_length=3)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    confidence = models.DecimalField(default=0.0, max_digits=3, decimal_places=2)


class Channel(models.Model):
    """Model representing Telegram chat that is related to unit.

    Channels only relate to OperationalUnit instances.

    Fields:
        - unit: Related OperationalUnit instance.
        - chat_id: The chat id of this channel.

    """
    unit = models.OneToOneField('OperationalUnit', on_delete=models.CASCADE, related_name='related_response_channel')
    chat_id = models.IntegerField(default=-1)


class Map(models.Model):
    """Model for saving a map of the operation area.

    Fields:
        - map_file: The actual file containing the map. Must be image.
        - top_left_mgrs_grid: The MGRS grid id of the top-left corner of the map.
        - top_left_mgrs_e: The Easting coordinate in MGRS of the top-left
                           corner of the map.
        - top_left_mgrs_n: The Northing coordinate in MGRS of the top-left
                           corner of the map.
        - bottom_right_mgrs_grid: The MGRS grid id of the bottom-right corner
                                  of the map.
        - bottom_right_mgrs_e: The Easting coordinate in MGRS of the bottom-right
                               corner of the map.
        - bottom_right_mgrs_n: The Northing coordinate in MGRS of the bottom-right
                               corner of the map.
    """
    map_file = models.ImageField(upload_to='uploads/maps/')
    top_left_mgrs_grid = models.CharField(default='35VLG', max_length=5)
    top_left_mgrs_e = models.IntegerField(default=0)
    top_left_mgrs_n = models.IntegerField(default=0)
    bottom_right_mgrs_grid = models.CharField(default='35VLG', max_length=5)
    bottom_right_mgrs_e = models.IntegerField(default=0)
    bottom_right_mgrs_n = models.IntegerField(default=0)

# Generated by Django 3.0.8 on 2020-08-15 10:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0004_auto_20200812_1630'),
    ]

    operations = [
        migrations.RenameField(
            model_name='observation',
            old_name='last_position_e',
            new_name='position_e',
        ),
        migrations.RenameField(
            model_name='observation',
            old_name='last_position_grid',
            new_name='position_grid',
        ),
        migrations.RenameField(
            model_name='observation',
            old_name='last_position_n',
            new_name='position_n',
        ),
        migrations.RemoveField(
            model_name='observation',
            name='callsign',
        ),
        migrations.RemoveField(
            model_name='observation',
            name='description',
        ),
        migrations.AddField(
            model_name='observation',
            name='source',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='observer', to='bot.OperationalUnit'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='location',
            name='source',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='locations', to='bot.OperationalUnit'),
        ),
        migrations.AlterField(
            model_name='observation',
            name='faction',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='detected_units', to='bot.Faction'),
        ),
    ]

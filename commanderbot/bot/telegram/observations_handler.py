"""Telegram related things for observation reporting."""

import time
import logging
from django.core.exceptions import ObjectDoesNotExist

from telegram.ext import (MessageHandler, Filters,)
from bot.constants import utils as const_utils
from bot.constants import observation_report as const

from bot.telegram.report_handler import ReportHandler
from bot.models import Observation, Faction, OperationalUnit
from commanderbot.settings import BOT_CONFIG
import bot.constants.unit_types as utypes
from bot.utils import LocationManager

logger = logging.getLogger('observation_report_handler')


class ObservationReportParser:
    """Parser for observation reports."""

    def __init__(self):
        """Initialize the parser."""
        self.observation_template = {
            "faction": None,
            "type": None,
            "direction": None,
            "distance": None,
            "movement": None,
            "size": None,
            "vehicles": None,
        }
        self.observation = self.observation_template
        self.object_ready = False

    def start_new_object(self):
        """Set `self.object_ready` to False and reset `self.observation`."""
        self.observation = self.observation_template
        self.object_ready = False

    def is_ready(self):
        self.object_ready = self._report_ready()
        return self.object_ready

    def get_report_as_obj(self, observer_id):
        """Get current report result as Observation object.

        Args:
            observer_id (int): The unit id that made the report.

        Returns:
            Observation: Report results as database Observation object.
        """
        try:
            unit = OperationalUnit.objects.get(chat_id_from=observer_id)
            mgrs = LocationManager()
            mgrs.mgrs_grid = unit.last_position_grid
            mgrs.mgrs_e = unit.last_position_e
            mgrs.mgrs_n = unit.last_position_n
            loc = mgrs.get_from(self.observation["direction"], self.observation["distance"])
            current_time = time.time()
            current = Observation.objects.create(
                faction=self.observation["faction"],
                type=self.observation["type"],
                position_grid=loc[0],
                position_e=loc[1],
                position_n=loc[2],
                movement_direction=self.observation["movement"],
                size=self.observation["size"],
                num_of_vehicles=self.observation["vehicles"],
                confidence=(unit.updated.timestamp() / current_time),
                source=unit
            )
            current.save()
            return current
        except ObjectDoesNotExist:
            logger.error(f"Unit with id {observer_id} could not be found when making observation report.")
            return None
        except Exception as e:
            logger.error(
                f"Could not make observation made by unit with id {observer_id}. " +
                f"Exception that occured was: {e}."
            )
            return None

    def _report_ready(self):
        ready = True
        for part in self.observation:
            ready = ready and self.observation[part] is not None
        return ready

    def set_faction(self, faction):
        """Set faction of the observation object.

        Args:
            faction (str): Faction in string format.

        Returns:
            bool: False if own faction given or was able to parse the data.
        """
        try:
            if faction == BOT_CONFIG['OWN-FACTION-NAME']:
                logger.warning("Observations cannot be created from own faction units")
                return False
            elif faction == BOT_CONFIG['ENEMY-FACTION-NAME']:
                self.observation["faction"] = Faction.objects.get(name=BOT_CONFIG['ENEMY-FACTION-NAME'])
                return True
            elif faction == BOT_CONFIG['CIVILIAN-FACTION-NAME']:
                self.observation["faction"] = Faction.objects.get(name=BOT_CONFIG['CIVILIAN-FACTION-NAME'])
                return True
            elif faction == BOT_CONFIG['YELLOW-FACTION-NAME']:
                self.observation["faction"] = Faction.objects.get(name=BOT_CONFIG['YELLOW-FACTION-NAME'])
                return True
            else:
                logger.warning("Unknown faction given: {}".format(faction))
                return False
        except ObjectDoesNotExist:
            logger.error("Faction with name {} does not exist".format(faction))
            return False

    def set_type(self, unit_type):
        """Set the type of the observation (e.g. Inf, Mech, Civ, etc.).

        Args:
            unit_type (str): Type of the unit as predefined string.

        Returns:
            bool: False if failed to parse data or incorrect unit given.
        """
        if unit_type == "Infantry":
            self.observation["type"] = utypes.INFANTRY
        elif unit_type == "Mechanized":
            self.observation["type"] = utypes.MECHANIZED
        elif unit_type == "Recon":
            self.observation["type"] = utypes.RECON
        elif unit_type == "PsyOps":
            self.observation["type"] = utypes.PSYOPS
        elif unit_type == "Commander/HVT":
            self.observation["type"] = utypes.COMMANDER
        elif unit_type == "Building":
            self.observation["type"] = utypes.BUILDING
        elif unit_type == "Offgame":
            self.observation["type"] = utypes.OFFGAME
        else:
            logger.warning("Unknown unit type: {}".format(unit_type))
            return False
        return True

    def set_direction(self, direction):
        """Set direction in which the observed unit is from the sender.

        Args:
            direction (str): Compass direction of the observed unit.

        Returns:
            bool: False if incorrect direction given.
        """
        if direction == "NW":
            self.observation["direction"] = 315
        elif direction == "N":
            self.observation["direction"] = 0
        elif direction == "NE":
            self.observation["direction"] = 45
        elif direction == "E":
            self.observation["direction"] = 90
        elif direction == "SE":
            self.observation["direction"] = 135
        elif direction == "S":
            self.observation["direction"] = 180
        elif direction == "SW":
            self.observation["direction"] = 225
        elif direction == "W":
            self.observation["direction"] = 270
        else:
            logger.warning("Bad direction given {}".format(direction))
            return False
        return True

    def set_distance(self, distance):
        """Set the distance to observed unit from the observer.

        Args:
            distance (str): Distance value in meters (preformatted to either
                            `100m` or `Over 500m`).

        Returns:
            bool: False if distance given in incorrect format.
        """
        try:
            if "Over" in distance:
                if float(distance.split(" ")[1].split("m")[0]) == 500:
                    self.observation["distance"] = 600
                else:
                    logger.warning("Bad distance value given {}".format(distance))
                    return False
            else:
                self.observation["distance"] = float(distance.split("m")[0])
            return True
        except IndexError:
            logger.warning("Bad distance value given {}".format(distance))
            return False
        except ValueError:
            logger.warning("Bad distance value given {}".format(distance))
            return False

    def set_movement(self, direction):
        """Set to which direction the observed unit is moving.

        Args:
            direction (str): Compass direction or stationary.

        Returns:
            bool: False if incorrect format direction given.
        """
        if direction == "NW":
            self.observation["movement"] = 315
        elif direction == "N":
            self.observation["movement"] = 0
        elif direction == "NE":
            self.observation["movement"] = 45
        elif direction == "E":
            self.observation["movement"] = 90
        elif direction == "SE":
            self.observation["movement"] = 135
        elif direction == "S":
            self.observation["movement"] = 180
        elif direction == "SW":
            self.observation["movement"] = 225
        elif direction == "W":
            self.observation["movement"] = 270
        elif direction == "Stationary":
            self.observation["movement"] = -1
        else:
            logger.warning("Bad movement direction given {}".format(direction))
            return False
        return True

    def set_size(self, size):
        """Set the size of the observed unit.

        Args:
            size (str): Predefined format size, uses army terms.

        Returns:
            bool: False if size given in incorrect format.
        """
        if size == "1 man":
            self.observation["size"] = utypes.SINGLE
        elif size == "Patrol, 2-4 men":
            self.observation["size"] = utypes.PATROL
        elif size == "Squad, 5-12 men":
            self.observation["size"] = utypes.SQUAD
        elif size == "Platoon, 15-30 men":
            self.observation["size"] = utypes.PLATOON
        elif size == "Company, ~100 men":
            self.observation["size"] = utypes.COMPANY
        elif size == "Battalion, ~300 men":
            self.observation["size"] = utypes.BATTALION
        else:
            logger.warning("Bad size given {}".format(size))
            return False
        return True

    def set_vehicles(self, amount):
        """Set amount of vehicles in the observed unit.

        Args:
            amount (str): Vehicle amount as string, must be convertible to integers.

        Returns:
            bool: False if given amount cannot be converted to integer.
        """
        try:
            self.observation["vehicles"] = int(amount[0])
            return True
        except ValueError:
            logger.warning("Bad amount of vehicles given {}".format(amount))
            return False


class ObservationReportHandler(ReportHandler):
    """Container for states of observation report handling."""

    def __init__(self, report_handler_func, custom_text_handler_func):
        """Initialize the states with appropriate functions.

        This initialization requires functions which handle the appropriate
        and the non-appropriate responses.

        Args:
            report_handler_func (func): Function that handles the response
                                        message in every state.
            custom_text_handler_func (func): Function that handles if something
                                             else is posted than appropriate
                                             report answer which matches the
                                             regex filter.
        """
        self.parser = ObservationReportParser()
        self.states = {
            1: ([
                MessageHandler(Filters.regex(const_utils.get_regex(const.OBS_REPORT, 1)), report_handler_func),
                MessageHandler(Filters.text, custom_text_handler_func)
            ], self.parser.set_faction),
            2: ([
                MessageHandler(Filters.regex(const_utils.get_regex(const.OBS_REPORT, 2)), report_handler_func),
                MessageHandler(Filters.text, custom_text_handler_func)
            ], self.parser.set_type),
            3: ([
                MessageHandler(Filters.regex(const_utils.get_regex(const.OBS_REPORT, 3)), report_handler_func),
                MessageHandler(Filters.text, custom_text_handler_func)
            ], self.parser.set_direction),
            4: ([
                MessageHandler(Filters.regex(const_utils.get_regex(const.OBS_REPORT, 4)), report_handler_func),
                MessageHandler(Filters.text, custom_text_handler_func)
            ], self.parser.set_distance),
            5: ([
                MessageHandler(Filters.regex(const_utils.get_regex(const.OBS_REPORT, 5)), report_handler_func),
                MessageHandler(Filters.text, custom_text_handler_func)
            ], self.parser.set_movement),
            6: ([
                MessageHandler(Filters.regex(const_utils.get_regex(const.OBS_REPORT, 6)), report_handler_func),
                MessageHandler(Filters.text, custom_text_handler_func)
            ], self.parser.set_size),
            7: ([
                MessageHandler(Filters.regex(const_utils.get_regex(const.OBS_REPORT, 7)), report_handler_func),
                MessageHandler(Filters.text, custom_text_handler_func)
            ], self.parser.set_vehicles),
        }
        self.current_state = 1
        super().__init__()

    def _report_ready(self):
        """Test if all needed items are in the report."""
        return self.parser.is_ready()

    def start_new_report(self):
        """Start new report."""
        self.current_state = 1
        self.parser.start_new_object()

    def save_current_report(self, observer_chat_id):
        return self.parser.get_report_as_obj(observer_chat_id)

    def get_current_state_number(self):
        """Return current state number or None if last state is reached."""
        if self.current_state > len(self.states.keys()):
            return None
        return self.current_state

    def get_current_state(self):
        """Get the current state of the report.

        Returns:
            list: List containing MessageHandler objects for that state.
                  Format:
                  `
                  [
                      MessageHandler(Filters...),
                      MessageHandler(Filters...),
                      ...
                  ]
                  `
        """
        return self.states[self.current_state][0]

    def put_data(self, data):
        """Input data to handler which is then parsed to desired object data.

        Args:
            data (str): The text data received through Telegram API.

        Returns:
            bool: Whether data was successfully parsed or not.
        """
        # check state
        # use correct parser function
        if not self.states[self.current_state][1](data):
            return False
        self.current_state += 1
        return True

    def is_ready(self, data):
        return (self.parser.is_ready() and self.current_state > len(self.states.keys()))

"""Main instance that runs the whole Telegram Bot."""

import logging

from django.db import IntegrityError
from django.core.exceptions import ObjectDoesNotExist
from telegram import TelegramError
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters,
                          ConversationHandler, PicklePersistence)
from telegram.ext import messagequeue as mq
from telegram.utils.request import Request
from telegram import ForceReply, ReplyKeyboardMarkup

from commanderbot.settings import (PRODUCTION_MODE_ON, RAW_MSG_LOG_PATH,
                                   BOT_CONFIG)

from bot.telegram.bot import TGBot
from bot.constants.observation_report import OBS_REPORT
from bot.constants.sitrep import SITREP
from bot.telegram.observations_handler import ObservationReportHandler
from bot.telegram.sitrep_handler import SitRepHandler
from bot.constants import menus as menus
from bot.constants.menus import BOT_COMMANDS
from bot.models import (OperationalUnit, Faction, Location)
from bot.constants import utils as const_utils
from bot.utils import LocationManager


logger = logging.getLogger('commanderbot')

(
    CHOOSING, REGISTRATION, OBSERVATION_REPORTING, SITUATION_REPORTING,
    REGISTRATION_WAITING_FOR_APPROVAL,
) = range(5)


class CommanderBot:
    """Main class for running the Telegram Commander Bot."""

    def __init__(self, token):
        """Initialize the commanderbot instance.

        Args:
            token (str): Telegram Bot API token.
        """
        self.pp = PicklePersistence(filename=BOT_CONFIG['PERSISTENCE_FILE'])

        if PRODUCTION_MODE_ON:
            # create message queue bot
            self.queue = mq.MessageQueue(
                all_burst_limit=25,
                all_time_limit_ms=1000,
                group_burst_limit=20,
                group_time_limit_ms=60000,
                exc_route=self._exception_handler,
                autostart=True
            )
            request = Request(con_pool_size=8)
            self.bot = TGBot(token, request=request, mqueue=self.queue)
            # create update handler
            self.updater = Updater(bot=self.bot, persistence=self.pp, use_context=True)
        else:
            self.updater = Updater(token=token, persistence=self.pp, use_context=True)

        # create observation report handler
        self.obsrep_handler = ObservationReportHandler(self._observation_report, self._custom_report_text)
        self.obs_report_state = self.obsrep_handler.get_current_state_number()
        # create sitrep handler
        self.sitrep_handler = SitRepHandler(self._sitrep, self._custom_report_text)
        self.sitrep_state = self.sitrep_handler.get_current_state_number()
        # create the states
        self.states = {
            CHOOSING: [
                MessageHandler(Filters.regex('^(Observation report)$'), self._observation_report_start),
                MessageHandler(Filters.regex('^(SITREP)$'), self._sitrep_start),
                MessageHandler(Filters.regex('^(Available commands)$'), self._help),
                MessageHandler(Filters.location, self._location),
                MessageHandler(Filters.text, self._customTextMainMenu),
                MessageHandler(Filters.document, self._customDocumentMainMenu),
                MessageHandler(Filters.photo, self._customPhotoMainMenu),
                MessageHandler(Filters.sticker, self._customStickerMainMenu),
                MessageHandler(Filters.video, self._customVideoMainMenu),
                MessageHandler(Filters.voice, self._customVoiceMainMenu),
            ],
            REGISTRATION: [
                MessageHandler(Filters.regex('^(Register)$'), self._registration_menu2),
                MessageHandler(Filters.text, self._registration),
            ],
            OBSERVATION_REPORTING: [
                MessageHandler(Filters.text, self._observation_report),
            ],
            SITUATION_REPORTING: [
                MessageHandler(Filters.text, self._sitrep),
            ],
            REGISTRATION_WAITING_FOR_APPROVAL: [
                MessageHandler(Filters.text, self._registration_waiting)
            ]
        }
        self.current_state = CHOOSING
        # create the conversation manager
        self.conv_manager = ConversationHandler(
            entry_points=[CommandHandler('start', self.start)],
            states=self.states,
            fallbacks=[MessageHandler(Filters.regex('^Help$'), self._help)],
            name='commanderbot_conversation',
            persistent=True
        )
        self.private_chat_group_handler = MessageHandler(Filters.group, self._groupMsgRelay)

        self.updater.dispatcher.add_handler(self.private_chat_group_handler)
        self.updater.dispatcher.add_handler(self.conv_manager)
        logger.info("CommanderBot initialized.")

        # TODO: make this function as utility (def create_logger(...))
        self.raw_msg_logger = logging.getLogger('commanderbot.raw_msg_logger')
        self.formatter = logging.Formatter(
            '%(asctime)s %(name)-12s %(levelname)-8s %(message)s', datefmt='%d-%m-%Y %H:%M:%S')
        self.fileHandler = logging.FileHandler(RAW_MSG_LOG_PATH, mode='a')
        self.fileHandler.setFormatter(self.formatter)
        self.streamHandler = logging.StreamHandler()
        self.streamHandler.setFormatter(self.formatter)
        self.raw_msg_logger.setLevel(logging.INFO)
        self.raw_msg_logger.addHandler(self.fileHandler)
        self.raw_msg_logger.addHandler(self.streamHandler)

    def start_bot(self):
        """Start the bot polling.

        This function exits since the called telegram.ext package function
        starts a separate thread.
        """
        logger.info("Started bot.")
        self.updater.start_polling()

    def stop_bot(self):
        """Stop the bot operation."""
        logger.info("Stopped bot.")
        self.updater.stop()

    def _observation_report_start(self, update, context):
        """Start the observation reporting."""
        self.obs_report_state = self.obsrep_handler.get_current_state_number()
        question_keyboard = ReplyKeyboardMarkup(OBS_REPORT[self.obs_report_state][1])
        update.message.reply_text(OBS_REPORT[self.obs_report_state][0], reply_markup=question_keyboard)
        self.current_state = OBSERVATION_REPORTING
        return OBSERVATION_REPORTING

    def _observation_report(self, update, context):
        """Run the observation reporting state functionality.

        Returns:
            int: Integer representing the current state.
        """
        if 'Cancel' in update.message.text:
            self.obsrep_handler.start_new_report()
            self.obs_report_state = self.obsrep_handler.get_current_state_number()
            return self._main_menu(update, context, "Canceled Observation report")
        data = update.message.text
        if not self.obsrep_handler.put_data(data):
            logger.warning("Failed to parse observation data: {} for state: {}. User id: {}".format(
                data, self.obs_report_state, update.effective_chat.id))
        self.obs_report_state = self.obsrep_handler.get_current_state_number()
        if self.obs_report_state is None:
            obj = self.obsrep_handler.save_current_report(update.effective_chat.id)
            self.obsrep_handler.start_new_report()
            self.obs_report_state = self.obsrep_handler.get_current_state_number()
            if obj is None:
                return self._main_menu(update, context, "Failed to save report. Please consult TOC about this issue.")
            else:
                return self._main_menu(update, context, "Report sent. Thank you for your service!")
        question_keyboard = ReplyKeyboardMarkup(OBS_REPORT[self.obs_report_state][1])
        update.message.reply_text(OBS_REPORT[self.obs_report_state][0], reply_markup=question_keyboard)
        self.current_state = OBSERVATION_REPORTING
        return OBSERVATION_REPORTING

    def _sitrep_start(self, update, context):
        self.sitrep_state = self.sitrep_handler.get_current_state_number()
        question_keyboard = ReplyKeyboardMarkup(SITREP[self.sitrep_state][1])
        update.message.reply_text(SITREP[self.sitrep_state][0], reply_markup=question_keyboard)
        self.current_state = SITUATION_REPORTING
        return SITUATION_REPORTING

    def _sitrep(self, update, context):
        if 'Cancel' in update.message.text:
            self.sitrep_handler.start_new_report()
            self.sitrep_state = self.sitrep_handler.get_current_state_number()
            return self._main_menu(update, context, "Canceled Situation report")
        data = update.message.text
        if not self.sitrep_handler.put_data(data):
            logger.warning("Failed to parse sitrep data: {} for state: {}. User id: {}".format(
                data, self.sitrep_state, update.effective_user.id))
        self.sitrep_state = self.sitrep_handler.get_current_state_number()
        if self.sitrep_state is None:
            ok = self.sitrep_handler.save_current_report(update.effective_chat.id)
            self.sitrep_handler.start_new_report()
            self.sitrep_state = self.sitrep_handler.get_current_state_number()
            if ok:
                return self._main_menu(
                    update, context, "Report sent. Thank you for your status info!")
            else:
                logger.error(
                    f"Tried to save sitrep to unit with id {update.effective_chat.id}, but failed."
                )
                return self._main_menu(
                    update, context, "Failed to send status info. Please consult TOC about this problem.")
        question_keyboard = ReplyKeyboardMarkup(SITREP[self.sitrep_state][1])
        update.message.reply_text(SITREP[self.sitrep_state][0], reply_markup=question_keyboard)
        self.current_state = SITUATION_REPORTING
        return SITUATION_REPORTING

    def _custom_report_text(self, update, context):
        # TODO: Verify this works, it might be that the
        # data = update.message.text does not work anymore after this
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Please select either Cancel or some button below")
        return self.current_state

    def _help(self, update, context):
        help_str = "CommanderBot\nAvailable commands:\n"
        for key in BOT_COMMANDS:
            help_str += (key + " - " + BOT_COMMANDS[key] + "\n\n")
        return self._main_menu(update, context, help_str)

    # ██╗      ██████╗  ██████╗ █████╗ ████████╗██╗ ██████╗ ███╗   ██╗
    # ██║     ██╔═══██╗██╔════╝██╔══██╗╚══██╔══╝██║██╔═══██╗████╗  ██║
    # ██║     ██║   ██║██║     ███████║   ██║   ██║██║   ██║██╔██╗ ██║
    # ██║     ██║   ██║██║     ██╔══██║   ██║   ██║██║   ██║██║╚██╗██║
    # ███████╗╚██████╔╝╚██████╗██║  ██║   ██║   ██║╚██████╔╝██║ ╚████║
    # ╚══════╝ ╚═════╝  ╚═════╝╚═╝  ╚═╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝

    def _location(self, update, context):
        location = LocationManager()
        location.init_from_lat_long_ts(
            update.message.location.latitude,
            update.message.location.longitude,
            update.message.date
        )
        try:
            unit = OperationalUnit.objects.get(
                chat_id_from=update.effective_chat.id)
            unit.last_position_grid = location.mgrs_grid
            unit.last_position_e = location.mgrs_e
            unit.last_position_n = location.mgrs_n
            unit.save()
            loc_obj = Location.objects.create(
                latitude=location.latitude,
                longitude=location.longitude,
                mgrs_grid=location.mgrs_grid,
                mgrs_e=location.mgrs_e,
                mgrs_n=location.mgrs_n,
                source=unit
            )
            loc_obj.save()
            return self._main_menu(update, context, f"Your location was {location.mgrs_grid} {location.mgrs_e} {location.mgrs_n}")
        except ObjectDoesNotExist:
            logger.error(
                f"Unit shared location but unit with chat id {update.effective_chat.id} does not exist."
            )
            return self._main_menu(update, context, "Failed to send location. Please consult TOC about this error.")
        except Exception as e:
            logger.error(
                (
                    f"Following exception occured when unit with id {update.effective_chat.id} tried to send" +
                    f"location: {e}"
                )
            )
            return self._main_menu(
                update, context, "Failed to send location. Please try again or constult TOC about the error.")

    # ████████╗███████╗██╗  ██╗████████╗
    # ╚══██╔══╝██╔════╝╚██╗██╔╝╚══██╔══╝
    #    ██║   █████╗   ╚███╔╝    ██║
    #    ██║   ██╔══╝   ██╔██╗    ██║
    #    ██║   ███████╗██╔╝ ██╗   ██║
    #    ╚═╝   ╚══════╝╚═╝  ╚═╝   ╚═╝

    def _customTextMainMenu(self, update, context):
        log_str = (
            f"User: {update.message.from_user.first_name}, {update.message.from_user.last_name}, " +
            f"{update.message.from_user.username}, {update.message.from_user.id}; " +
            f"Msg: {update.message.text}"
        )
        self.raw_msg_logger.info(log_str)
        try:
            unit = OperationalUnit.objects.get(chat_id_from=update.effective_chat.id)
            context.bot.send_message(
                chat_id=unit.chat_id_to,
                text=(
                    f"{unit.callsign} sent:\n" +
                    update.message.text
                )
            )
            return self._main_menu(update, context, "Relayed: {}".format(update.message.text))
        except ObjectDoesNotExist:
            logger.error(
                (
                    f"User with id {update.effective_chat.id} sent a message but this user is not " +
                    "assigned to any OperationalUnit. This should not happen."
                )
            )
            return self._main_menu(
                update, context, "Failed to relay your message. Please consult TOC for information.")
        except TelegramError as e:
            logger.error(
                (
                    f"User with id {update.effective_chat.id} sent a message but that message could not " +
                    "be relayed to another chat. Please check that the bot is in correct chat. " +
                    f"Error was {e}."
                )
            )
            return self._main_menu(
                update, context, "Failed to relay your message. Please consult TOC for information.")
        except Exception as e:
            logger.error(
                (
                    f"Following exception occured when user with id {update.effective_chat.id} tried to send" +
                    f"message: {update.message.text} to another chat: {e}"
                )
            )
            return self._main_menu(
                update, context, "Failed to relay your message. Please consult TOC for information.")

    # ██████╗  ██████╗  ██████╗██╗   ██╗███╗   ███╗███████╗███╗   ██╗████████╗███████╗
    # ██╔══██╗██╔═══██╗██╔════╝██║   ██║████╗ ████║██╔════╝████╗  ██║╚══██╔══╝██╔════╝
    # ██║  ██║██║   ██║██║     ██║   ██║██╔████╔██║█████╗  ██╔██╗ ██║   ██║   ███████╗
    # ██║  ██║██║   ██║██║     ██║   ██║██║╚██╔╝██║██╔══╝  ██║╚██╗██║   ██║   ╚════██║
    # ██████╔╝╚██████╔╝╚██████╗╚██████╔╝██║ ╚═╝ ██║███████╗██║ ╚████║   ██║   ███████║
    # ╚═════╝  ╚═════╝  ╚═════╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝

    def _customDocumentMainMenu(self, update, context):
        log_str = (
            f"User: {update.message.from_user.first_name}, {update.message.from_user.last_name}, " +
            f"{update.message.from_user.username}, {update.message.from_user.id}; " +
            f"Msg: {update.message.document.file_name}"
        )
        self.raw_msg_logger.info(log_str)
        try:
            unit = OperationalUnit.objects.get(chat_id_from=update.effective_chat.id)
            caption = update.message.caption
            if caption is None:
                caption = ""
            context.bot.send_document(
                chat_id=unit.chat_id_to,
                document=update.message.document,
                filename=update.message.document.file_name,
                caption=f"{unit.callsign} sent: {caption}"
            )
            return self._main_menu(update, context, f"Relayed: {update.message.document.file_name}")
        except ObjectDoesNotExist:
            logger.error(
                (
                    f"User with id {update.effective_chat.id} sent a document but this user is not " +
                    "assigned to any OperationalUnit. This should not happen."
                )
            )
            return self._main_menu(
                update, context, "Failed to relay your document. Please consult TOC for information.")
        except TelegramError as e:
            logger.error(
                (
                    f"User with id {update.effective_chat.id} sent a document but that document could not " +
                    "be relayed to another chat. Please check that the bot is in correct chat. " +
                    f"Error was {e}."
                )
            )
            return self._main_menu(
                update, context, "Failed to relay your document. Please consult TOC for information.")
        except Exception as e:
            logger.error(
                (
                    f"Following exception occured when user with id {update.effective_chat.id} tried to send" +
                    f"document: {update.message.document.file_name} to another chat: {e}"
                )
            )
            return self._main_menu(
                update, context, "Failed to relay your document. Please consult TOC for information.")

    # ██████╗ ██╗  ██╗ ██████╗ ████████╗ ██████╗ ███████╗
    # ██╔══██╗██║  ██║██╔═══██╗╚══██╔══╝██╔═══██╗██╔════╝
    # ██████╔╝███████║██║   ██║   ██║   ██║   ██║███████╗
    # ██╔═══╝ ██╔══██║██║   ██║   ██║   ██║   ██║╚════██║
    # ██║     ██║  ██║╚██████╔╝   ██║   ╚██████╔╝███████║
    # ╚═╝     ╚═╝  ╚═╝ ╚═════╝    ╚═╝    ╚═════╝ ╚══════╝

    def _customPhotoMainMenu(self, update, context):
        log_str = (
            f"User: {update.message.from_user.first_name}, {update.message.from_user.last_name}, " +
            f"{update.message.from_user.username}, {update.message.from_user.id}; " +
            f"Photo"
        )
        self.raw_msg_logger.info(log_str)
        try:
            unit = OperationalUnit.objects.get(chat_id_from=update.effective_chat.id)
            caption = update.message.caption
            if caption is None:
                caption = ""
            context.bot.send_photo(
                chat_id=unit.chat_id_to,
                photo=update.message.photo[len(update.message.photo)-1].file_id,
                caption=f"{unit.callsign} sent: {caption}"
            )
            return self._main_menu(update, context, "Relayed your photo")
        except ObjectDoesNotExist:
            logger.error(
                (
                    f"User with id {update.effective_chat.id} sent a photo but this user is not " +
                    "assigned to any OperationalUnit. This should not happen."
                )
            )
            return self._main_menu(
                update, context, "Failed to relay your photo. Please consult TOC for information.")
        except TelegramError as e:
            logger.error(
                (
                    f"User with id {update.effective_chat.id} sent a photo but that photo could not " +
                    "be relayed to another chat. Please check that the bot is in correct chat. " +
                    f"Error was {e}."
                )
            )
            return self._main_menu(
                update, context, "Failed to relay your photo. Please consult TOC for information.")
        except Exception as e:
            logger.error(
                (
                    f"Following exception occured when user with id {update.effective_chat.id} tried to send" +
                    f"photo to another chat: {e}"
                )
            )
            return self._main_menu(
                update, context, "Failed to relay your photo. Please consult TOC for information.")

    # ███████╗████████╗██╗ ██████╗██╗  ██╗███████╗██████╗ ███████╗
    # ██╔════╝╚══██╔══╝██║██╔════╝██║ ██╔╝██╔════╝██╔══██╗██╔════╝
    # ███████╗   ██║   ██║██║     █████╔╝ █████╗  ██████╔╝███████╗
    # ╚════██║   ██║   ██║██║     ██╔═██╗ ██╔══╝  ██╔══██╗╚════██║
    # ███████║   ██║   ██║╚██████╗██║  ██╗███████╗██║  ██║███████║
    # ╚══════╝   ╚═╝   ╚═╝ ╚═════╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚══════╝

    def _customStickerMainMenu(self, update, context):
        log_str = (
            f"User: {update.message.from_user.first_name}, {update.message.from_user.last_name}, " +
            f"{update.message.from_user.username}, {update.message.from_user.id}; " +
            f"Sticker"
        )
        self.raw_msg_logger.info(log_str)
        try:
            unit = OperationalUnit.objects.get(chat_id_from=update.effective_chat.id)
            context.bot.send_sticker(
                chat_id=unit.chat_id_to,
                sticker=update.message.sticker.file_id
            )
            return self._main_menu(update, context, "Relayed your sticker")
        except ObjectDoesNotExist:
            logger.error(
                (
                    f"User with id {update.effective_chat.id} sent a sticker but this user is not " +
                    "assigned to any OperationalUnit. This should not happen."
                )
            )
            return self._main_menu(
                update, context, "Failed to relay your sticker. Please consult TOC for information.")
        except TelegramError as e:
            logger.error(
                (
                    f"User with id {update.effective_chat.id} sent a sticker but that sticker could not " +
                    "be relayed to another chat. Please check that the bot is in correct chat. " +
                    f"Error was {e}."
                )
            )
            return self._main_menu(
                update, context, "Failed to relay your sticker. Please consult TOC for information.")
        except Exception as e:
            logger.error(
                (
                    f"Following exception occured when user with id {update.effective_chat.id} tried to send" +
                    f"sticker to another chat: {e}"
                )
            )
            return self._main_menu(
                update, context, "Failed to relay your sticker. Please consult TOC for information.")

    # ██╗   ██╗██╗██████╗ ███████╗ ██████╗
    # ██║   ██║██║██╔══██╗██╔════╝██╔═══██╗
    # ██║   ██║██║██║  ██║█████╗  ██║   ██║
    # ╚██╗ ██╔╝██║██║  ██║██╔══╝  ██║   ██║
    #  ╚████╔╝ ██║██████╔╝███████╗╚██████╔╝
    #   ╚═══╝  ╚═╝╚═════╝ ╚══════╝ ╚═════╝

    def _customVideoMainMenu(self, update, context):
        log_str = (
            f"User: {update.message.from_user.first_name}, {update.message.from_user.last_name}, " +
            f"{update.message.from_user.username}, {update.message.from_user.id}; " +
            f"Video"
        )
        self.raw_msg_logger.info(log_str)
        try:
            unit = OperationalUnit.objects.get(chat_id_from=update.effective_chat.id)
            caption = update.message.caption
            if caption is None:
                caption = ""
            context.bot.send_video(
                chat_id=unit.chat_id_to,
                video=update.message.video.file_id,
                timeout=120,
                caption=f"{unit.callsign} sent: {caption}"
            )
            return self._main_menu(update, context, "Relayed your video")
        except ObjectDoesNotExist:
            logger.error(
                (
                    f"User with id {update.effective_chat.id} sent a video but this user is not " +
                    "assigned to any OperationalUnit. This should not happen."
                )
            )
            return self._main_menu(
                update, context, "Failed to relay your video. Please consult TOC for information.")
        except TelegramError as e:
            logger.error(
                (
                    f"User with id {update.effective_chat.id} sent a video but that video could not " +
                    "be relayed to another chat. Please check that the bot is in correct chat. " +
                    f"Error was {e}."
                )
            )
            return self._main_menu(
                update, context, "Failed to relay your video. Please consult TOC for information.")
        except Exception as e:
            logger.error(
                (
                    f"Following exception occured when user with id {update.effective_chat.id} tried to send" +
                    f"video to another chat: {e}"
                )
            )
            return self._main_menu(
                update, context, "Failed to relay your video. Please consult TOC for information.")

    # ██╗   ██╗ ██████╗ ██╗ ██████╗███████╗
    # ██║   ██║██╔═══██╗██║██╔════╝██╔════╝
    # ██║   ██║██║   ██║██║██║     █████╗
    # ╚██╗ ██╔╝██║   ██║██║██║     ██╔══╝
    #  ╚████╔╝ ╚██████╔╝██║╚██████╗███████╗
    #   ╚═══╝   ╚═════╝ ╚═╝ ╚═════╝╚══════╝

    def _customVoiceMainMenu(self, update, context):
        log_str = (
            f"User: {update.message.from_user.first_name}, {update.message.from_user.last_name}, " +
            f"{update.message.from_user.username}, {update.message.from_user.id}; " +
            f"Voice"
        )
        self.raw_msg_logger.info(log_str)
        try:
            unit = OperationalUnit.objects.get(chat_id_from=update.effective_chat.id)
            caption = update.message.caption
            if caption is None:
                caption = ""
            context.bot.send_voice(
                chat_id=unit.chat_id_to,
                voice=update.message.voice.file_id,
                timeout=120,
                caption=f"{unit.callsign} sent: {caption}"
            )
            return self._main_menu(update, context, "Relayed your voice message")
        except ObjectDoesNotExist:
            logger.error(
                (
                    f"User with id {update.effective_chat.id} sent a voice message but this user is not " +
                    "assigned to any OperationalUnit. This should not happen."
                )
            )
            return self._main_menu(
                update, context, "Failed to relay your voice message. Please consult TOC for information.")
        except TelegramError as e:
            logger.error(
                (
                    f"User with id {update.effective_chat.id} sent a voice message but that voice message could not " +
                    "be relayed to another chat. Please check that the bot is in correct chat. " +
                    f"Error was {e}."
                )
            )
            return self._main_menu(
                update, context, "Failed to relay your voice message. Please consult TOC for information.")
        except Exception as e:
            logger.error(
                (
                    f"Following exception occured when user with id {update.effective_chat.id} tried to send" +
                    f"voice message to another chat: {e}"
                )
            )
            return self._main_menu(
                update, context, "Failed to relay your voice message. Please consult TOC for information.")

    #  ██████╗ ██████╗  ██████╗ ██╗   ██╗██████╗          ██╗       ██████╗██╗  ██╗ █████╗ ████████╗
    # ██╔════╝ ██╔══██╗██╔═══██╗██║   ██║██╔══██╗         ╚██╗     ██╔════╝██║  ██║██╔══██╗╚══██╔══╝
    # ██║  ███╗██████╔╝██║   ██║██║   ██║██████╔╝    █████╗╚██╗    ██║     ███████║███████║   ██║
    # ██║   ██║██╔══██╗██║   ██║██║   ██║██╔═══╝     ╚════╝██╔╝    ██║     ██╔══██║██╔══██║   ██║
    # ╚██████╔╝██║  ██║╚██████╔╝╚██████╔╝██║              ██╔╝     ╚██████╗██║  ██║██║  ██║   ██║
    #  ╚═════╝ ╚═╝  ╚═╝ ╚═════╝  ╚═════╝ ╚═╝              ╚═╝       ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝

    def send(self, update, context, chat_id, callsign):
        """Return True if supported feature."""
        if update.message.text is not None:
            context.bot.send_message(chat_id=chat_id, text=update.message.text)
            log_str = (
                f"Group: {update.effective_chat.id}, {update.effective_chat.title} sent following message to " +
                f"unit {callsign}: {update.message.text}"
            )
            self.raw_msg_logger.info(log_str)
        elif update.message.voice is not None:
            caption = update.message.caption
            if caption is None:
                caption = ""
            context.bot.send_voice(chat_id=chat_id, voice=update.message.voice.file_id, caption=f"{caption}")
            log_str = (
                f"Group: {update.effective_chat.id}, {update.effective_chat.title} sent following voice msg to " +
                f"unit {callsign}: {update.message.voice.file_id}"
            )
            self.raw_msg_logger.info(log_str)
        elif update.message.document is not None:
            caption = update.message.caption
            if caption is None:
                caption = ""
            context.bot.send_document(chat_id=chat_id, document=update.message.document.file_id, caption=f"{caption}")
            log_str = (
                f"Group: {update.effective_chat.id}, {update.effective_chat.title} sent following document to " +
                f"unit {callsign}: {update.message.document.file_name}"
            )
            self.raw_msg_logger.info(log_str)
        elif update.message.sticker is not None:
            context.bot.send_sticker(chat_id=chat_id, sticker=update.message.sticker.file_id)
            log_str = (
                f"Group: {update.effective_chat.id}, {update.effective_chat.title} sent sticker to unit {callsign}"
            )
            self.raw_msg_logger.info(log_str)
        elif update.message.video is not None:
            caption = update.message.caption
            if caption is None:
                caption = ""
            context.bot.send_video(
                chat_id=chat_id, video=update.message.video.file_id, caption=f"{caption}", timeout=120
            )
            log_str = (
                f"Group: {update.effective_chat.id}, {update.effective_chat.title} sent video to unit {callsign}"
            )
            self.raw_msg_logger.info(log_str)
        elif update.message.photo is not None:
            caption = update.message.caption
            if caption is None:
                caption = ""
            context.bot.send_photo(
                chat_id=chat_id,
                photo=update.message.photo[len(update.message.photo)-1].file_id,
                caption=f"{caption}"
            )
            log_str = (
                f"Group: {update.effective_chat.id}, {update.effective_chat.title} sent photo to unit {callsign}"
            )
            self.raw_msg_logger.info(log_str)
        else:
            return False
        return True

    def _groupMsgRelay(self, update, context):
        # TODO: create list of group ids to ignore and then just skip this function if
        # update.effective_chat.id is in that list
        try:
            callsign = const_utils.get_key_based_on_value(BOT_CONFIG['POSSIBLE_CALLSIGNS'], update.effective_chat.id)
            if callsign is None:
                logger.error(
                    f"No callsign found in settings.POSSIBLE_CALLSIGNS for chat id {update.effective_chat.id}, " +
                    f"{update.effective_chat.title}. Might be that the bot is in useless group."
                )
                return
            unit = OperationalUnit.objects.get(callsign=callsign)
            if not self.send(update, context, unit.chat_id_from, callsign):
                logger.warning(
                    f"Unsupported message type from group chat ({update.effective_chat.id}) " +
                    f"to private chat with {callsign}!"
                )
            return
        except ObjectDoesNotExist:
            logger.error(
                f"No unit found in with callsign: {callsign} for chat id {update.effective_chat.id}. " +
                "This indicates error in database. Check API.")
            update.message.reply_text(
                "Database error. Could not send the message to wanted unit. Check logs on the server!")
            return
        except TelegramError as e:
            logger.error(
                (
                    f"Failed to send a message to unit {callsign} from chat {update.effective_chat.id}. " +
                    f"Error was: {e}."
                )
            )
            update.message.reply_text("Could not send the message to wanted unit. Check logs on the server!")
            return
        except Exception as e:
            logger.error(
                (
                    f"Following exception occured when chat with id {update.effective_chat.id} tried to send " +
                    f"message: {update.message.text} to unit: {e}"
                )
            )
            update.message.reply_text("Could not send the message to wanted unit. Check logs on the server!")
            return

    def _main_menu(self, update, context, text_to_show):
        """Return to main menu of the bot.

        Args:
            update (object): Telegram Update object.
            context (object): Telegram conversation context object.
            text_to_show (str): Text that is send to user as message prior to
                                showing the menu.

        Returns:
            int: The state number (main menu state).
        """
        report_keyboard = ReplyKeyboardMarkup(menus.MAIN_MENU)
        update.message.reply_text(text_to_show, reply_markup=report_keyboard)
        self.current_state = CHOOSING
        return self.current_state

    def _registration_menu(self, update, context):
        """Handle registration state."""
        report_keyboard = ReplyKeyboardMarkup(menus.REGISTER_MENU)
        update.message.reply_text("You must register before you can use the bot", reply_markup=report_keyboard)
        self.current_state = REGISTRATION
        return self.current_state

    def _registration_menu2(self, update, context):
        logger.info(
            "User (id %d) requested registration to the sytem. The chat id is %d." % (
                update.effective_user.id, update.effective_chat.id))
        callsign_prompt = ForceReply()
        update.message.reply_text("Please provide your callsign.", reply_markup=callsign_prompt)
        self.current_state = REGISTRATION
        return self.current_state

    def _registration_reply(self, update, context, text, menu, state):
        report_keyboard = ReplyKeyboardMarkup(menu)
        update.message.reply_text(text, reply_markup=report_keyboard)
        self.current_state = state
        return self.current_state

    def _registration(self, update, context):
        """Handle actual user registration."""
        try:
            callsign = update.message.text
            unit, created = OperationalUnit.objects.get_or_create(
                callsign=callsign,
                description=f"Unit {callsign}",
                chat_id_from=update.effective_chat.id,
                partof=Faction.objects.get(name=BOT_CONFIG["OWN-FACTION-NAME"])
            )
            if not created:
                # user has been already registered to the given callsign
                logger.info(
                    f"User {update.effective_chat.id} was already registered to the system. " +
                    "Passed the user directly to the main menu."
                )
                return self._main_menu(update, context, f"Welcome back to the grid {unit.callsign}.")
            text_to_send = (
                f"Sent registration request for callsign {unit.callsign}. " +
                "TOC personnel will now review your request. Stand by."
            )
            return self._registration_reply(
                update, context, text_to_send, menus.REGISTER_WAITING_MENU, REGISTRATION_WAITING_FOR_APPROVAL)
        except IntegrityError as e:
            if 'UNIQUE constraint' in e.args[0]:
                logger.warning(
                    (
                        f"Incorrect user input: User {update.effective_user.id} sent registration request with " +
                        f"{callsign}, but this user has been registered to another callsign. " +
                        f"Exception that occured was {e}"
                    )
                )
                text_to_send = (
                    f"You tried to register to {callsign}, but either your user has been " +
                    "registered to another callsign or that callsign is already taken. " +
                    "Please consult TOC regarding this issue."
                )
                return self._registration_reply(
                    update, context, text_to_send, menus.REGISTER_MENU, REGISTRATION)
            else:
                logger.warning(
                    (
                        "Incorrect user input: User {} failed to reply properly to callsign request. " +
                        "The message text was {}. Exception that occured was {}"
                    ).format(update.effective_user.id, update.message.text, e)
                )
                text_to_send = "Bad input, please try again."
                return self._registration_reply(
                    update, context, text_to_send, menus.REGISTER_MENU, REGISTRATION)
        except Exception as e:
            logger.warning(
                (
                    "Incorrect user input: User {} failed to reply properly to callsign request. " +
                    "The message text was {}. Exception that occured was {}"
                ).format(update.effective_user.id, update.message.text, e)
            )
            text_to_send = "Bad input, please try again."
            return self._registration_reply(
                update, context, text_to_send, menus.REGISTER_MENU, REGISTRATION)

    def _registration_waiting(self, update, context):
        try:
            unit = OperationalUnit.objects.get(chat_id_from=update.effective_chat.id)
            if unit.approved:
                return self._main_menu(update, context, "Welcome to the grid.")
            else:
                report_keyboard = ReplyKeyboardMarkup(menus.REGISTER_WAITING_MENU)
                update.message.reply_text(
                    "Unfortunately TOC has not yet processed your request.", reply_markup=report_keyboard)
                self.current_state = REGISTRATION_WAITING_FOR_APPROVAL
                return self.current_state
        except ObjectDoesNotExist:
            report_keyboard = ReplyKeyboardMarkup(menus.REGISTER_MENU)
            update.message.reply_text(
                "Unfortunately TOC did not approve your request. Please consult TOC for the reason for this.",
                reply_markup=report_keyboard
            )
            self.current_state = REGISTRATION
            return self.current_state
        except Exception as e:
            logger.error((
                f"Following error occured when processing user (id: {update.effective_chat.id}) " +
                f"registration waiting request: {e}"
            ))
            report_keyboard = ReplyKeyboardMarkup(menus.REGISTER_WAITING_MENU)
            update.message.reply_text(
                "Unfortunately TOC has not yet processed your request.", reply_markup=report_keyboard)
            self.current_state = REGISTRATION_WAITING_FOR_APPROVAL
            return self.current_state

    def start(self, update, context):
        """Bot `start` API command.

        This command starts the conversation with the bot.
        """
        logger.info(
            "User (id %d) started a chat with the bot. The chat id is %d." %
            (update.effective_user.id, update.effective_chat.id))
        return self._registration_menu(update, context)

    def _exception_handler(self, exception):
        logger.error(f"Exception raised by MessageQueue: {exception}")
        return
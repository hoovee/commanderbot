"""Situation report handler."""

import logging
from telegram.ext import (MessageHandler, Filters,)
from django.core.exceptions import ObjectDoesNotExist

from bot.models import OperationalUnit
from bot.constants import unit_status as us
from bot.constants import utils as const_utils
from bot.constants import sitrep as const
from bot.telegram.report_handler import ReportHandler


logger = logging.getLogger('sitrep_report_handler')


class SitRepParser:
    """Parser for situation reports."""

    def __init__(self):
        """Initialize the parser."""
        self.sitrep_template = {
            "strength": None,
            "ammo": None,
            "water": None,
            "energy": None,
            "status": None
        }
        self.sitrep = self.sitrep_template
        self.object_ready = False

    def start_new_object(self):
        """Set self.object_ready to False and reset self.sitrep."""
        self.sitrep = self.sitrep_template
        self.object_ready = False

    def is_ready(self):
        self.object_ready = self._report_ready()
        return self.object_ready

    def save_report_to_unit(self, chat_id):
        """Save current report data to a existing unit.

        Args:
            chat_id (int): Chat id of the unit (unique).
        """
        try:
            unit = OperationalUnit.objects.get(chat_id_from=chat_id)
            unit.strength = self.sitrep["strength"]
            unit.ammo = self.sitrep["ammo"]
            unit.water = self.sitrep["water"]
            unit.energy = self.sitrep["energy"]
            unit.status = self.sitrep["status"]
            unit.save()
            return True
        except ObjectDoesNotExist:
            logger.error(f"OperationalUnit with id {chat_id} does not exist")
            return False

    def _report_ready(self):
        ready = True
        for part in self.sitrep:
            ready = ready and self.sitrep[part] is not None
        return ready

    def set_strength(self, strength):
        if strength == us.STATUS_QUANTITIES[0][1]:
            self.sitrep["strength"] = us.STATUS_QUANTITIES[0][0]
        elif strength == us.STATUS_QUANTITIES[1][1]:
            self.sitrep["strength"] = us.STATUS_QUANTITIES[1][0]
        elif strength == us.STATUS_QUANTITIES[2][1]:
            self.sitrep["strength"] = us.STATUS_QUANTITIES[2][0]
        else:
            logger.warning(f"Incorrect strength attribute given: {strength}")
            return False
        return True

    def set_ammo(self, ammo):
        if ammo == us.STATUS_QUANTITIES[0][1]:
            self.sitrep["ammo"] = us.STATUS_QUANTITIES[0][0]
        elif ammo == us.STATUS_QUANTITIES[1][1]:
            self.sitrep["ammo"] = us.STATUS_QUANTITIES[1][0]
        elif ammo == us.STATUS_QUANTITIES[2][1]:
            self.sitrep["ammo"] = us.STATUS_QUANTITIES[2][0]
        else:
            logger.warning(f"Incorrect ammo attribute given: {ammo}")
            return False
        return True

    def set_water(self, water):
        if water == us.STATUS_QUANTITIES[0][1]:
            self.sitrep["water"] = us.STATUS_QUANTITIES[0][0]
        elif water == us.STATUS_QUANTITIES[1][1]:
            self.sitrep["water"] = us.STATUS_QUANTITIES[1][0]
        elif water == us.STATUS_QUANTITIES[2][1]:
            self.sitrep["water"] = us.STATUS_QUANTITIES[2][0]
        else:
            logger.warning(f"Incorrect water attribute given: {water}")
            return False
        return True

    def set_energy(self, energy):
        if energy == us.STATUS_QUANTITIES[0][1]:
            self.sitrep["energy"] = us.STATUS_QUANTITIES[0][0]
        elif energy == us.STATUS_QUANTITIES[1][1]:
            self.sitrep["energy"] = us.STATUS_QUANTITIES[1][0]
        elif energy == us.STATUS_QUANTITIES[2][1]:
            self.sitrep["energy"] = us.STATUS_QUANTITIES[2][0]
        else:
            logger.warning(f"Incorrect energy attribute given: {energy}")
            return False
        return True

    def set_status(self, status):
        if status == us.MISSION_STATUS[0][1]:
            self.sitrep["status"] = us.MISSION_STATUS[0][0]
        elif status == us.MISSION_STATUS[1][1]:
            self.sitrep["status"] = us.MISSION_STATUS[1][0]
        elif status == us.MISSION_STATUS[2][1]:
            self.sitrep["status"] = us.MISSION_STATUS[2][0]
        elif status == us.MISSION_STATUS[3][1]:
            self.sitrep["status"] = us.MISSION_STATUS[3][0]
        else:
            logger.warning(f"Incorrect status attribute given: {status}")
            return False
        return True


class SitRepHandler(ReportHandler):
    """Handler for situation reports."""

    def __init__(self, report_handler_func, custom_text_handler_func):
        self.parser = SitRepParser()
        self.states = {
            1: ([
                MessageHandler(Filters.regex(const_utils.get_regex(const.SITREP, 1)), report_handler_func),
                MessageHandler(Filters.text, custom_text_handler_func)
            ], self.parser.set_strength),
            2: ([
                MessageHandler(Filters.regex(const_utils.get_regex(const.SITREP, 2)), report_handler_func),
                MessageHandler(Filters.text, custom_text_handler_func)
            ], self.parser.set_ammo),
            3: ([
                MessageHandler(Filters.regex(const_utils.get_regex(const.SITREP, 3)), report_handler_func),
                MessageHandler(Filters.text, custom_text_handler_func)
            ], self.parser.set_water),
            4: ([
                MessageHandler(Filters.regex(const_utils.get_regex(const.SITREP, 4)), report_handler_func),
                MessageHandler(Filters.text, custom_text_handler_func)
            ], self.parser.set_energy),
            5: ([
                MessageHandler(Filters.regex(const_utils.get_regex(const.SITREP, 5)), report_handler_func),
                MessageHandler(Filters.text, custom_text_handler_func)
            ], self.parser.set_status),
        }
        self.current_state = 1
        super().__init__()

    def _report_ready(self):
        self.parser.is_ready()

    def start_new_report(self):
        self.current_state = 1
        self.parser.start_new_object()

    def save_current_report(self, chat_id):
        return self.parser.save_report_to_unit(chat_id)

    def get_current_state_number(self):
        if self.current_state > len(self.states.keys()):
            return None
        return self.current_state

    def get_current_state(self):
        return self.states[self.current_state][0]

    def put_data(self, data):
        if not self.states[self.current_state][1](data):
            return False
        self.current_state += 1
        return True

    def is_ready(self, data):
        return (self.parser.is_ready() and self.current_state > len(self.states.keys()))

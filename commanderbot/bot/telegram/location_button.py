"""API for user's location retrieving functionality."""

from telegram import KeyboardButton


LOC_BUTTON = KeyboardButton(text="Location", request_location=True)

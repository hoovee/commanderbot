import logging
import telegram.bot
from telegram.ext import messagequeue as mq

logger_bot = logging.getLogger('commanderbot.bot')


class TGBot(telegram.bot.Bot):
    """API to Telegram that handles queueing messages, etc."""
    def __init__(self, *args, is_queued_def=True, mqueue=None, **kwargs):
        super(TGBot, self).__init__(*args, **kwargs)
        # below 2 attributes should be provided for decorator usage
        self._is_messages_queued_default = is_queued_def
        self._msg_queue = mqueue or mq.MessageQueue()
        logger_bot.info("Initialized TGBot instance")

    def __del__(self):
        try:
            self._msg_queue.stop()
        except Exception as e:
            logger_bot.warning(
                "Caught following exception when trying to end the bot: %s" % (repr(e)))
            pass

    @mq.queuedmessage
    def send_message(self, *args, **kwargs):
        """Wrapped method would accept new `queued` and `isgroup` OPTIONAL arguments"""
        return super(TGBot, self).send_message(*args, **kwargs)

"""Generic report handler.

This handler defines the API for all handlers. Every sub-handler should
implement the @abstractmethod decorated functions.
"""

from abc import ABC, abstractmethod


class ReportHandler(ABC):
    """Generic report handler base class."""

    def __init__(self):
        self.report_ready = False
        super().__init__()

    @abstractmethod
    def _report_ready(self):
        """Go through data objects of the report and check conditions."""
        pass

    @abstractmethod
    def start_new_report(self):
        """Start new report from the beginning.

        Will reset all report data and set report ready to false.
        You should call this using the super() call in the sub-class
        implementation (i.e. `super().start_new_report()`).
        """
        self.report_ready = False

    @abstractmethod
    def get_current_report(self, *args, **kwargs):
        pass

    @abstractmethod
    def get_next_state(self):
        """Return the next report state.

        The state is in form
        {
            state_number(int): (
                "Question or state description",
                [
                    ["Action for state", "Action"],
                    ["Another action for the state"],
                    ["Another action for the state", "Action"],
                    ...
                ]
            )
        }
        The return value is then the tuple behind the "state_number".
        """
        # todo: do not keep this here
        pass

    # todo: add data input method

    def is_report_ready(self):
        self.report_ready = self._report_ready()
        return self.report_ready

# from bot.reporting.report import ReportHandler
from bot.models import Observation, Faction
from commanderbot.settings import BOT_CONFIG
import bot.unit_types as utypes
# import bot.utils as butils
from bot.utils import LocationManager
import time


class ObservationReportParser:
    """Report parser for observation reports."""

    def __init__(self):
        self.observation_template = {
            "faction": None,
            "type": None,
            "direction": None,
            "distance": None,
            "movement": None,
            "size": None,
            "vehicles": None,
            "own_location": None,
        }
        self.observation = self.observation_template
        self.report_ready = False
        super.__init__()

    def start_new_report(self):
        """Start new object to report."""
        self.observation = self.observation_template
        self.report_ready = False

    def get_current_report(self, last_own_location):
        """Get current report result as Observation object.

        Args:
            last_own_location (Location): Last known location of the report maker.

        Returns:
            Observation: Report results as database Observation object.
        """
        mgrs = LocationManager(last_own_location).get_from(self.observation["direction"], self.observation["distance"])
        current_time = time.time()
        current = Observation.objects.create(
            callsign="",
            description="",
            size=self.observation["size"],
            type=self.observation["type"],
            last_position_grid=mgrs[0],
            last_position_e=mgrs[1],
            last_position_n=mgrs[2],
            confidence=(last_own_location.timestamp / current_time),
            movement_direction=self.observation["movement"],
            num_of_vehicles=self.observation["vehicles"],
        )
        current.faction = self.observation["faction"]
        current.save()
        return current

    def _report_ready(self):
        ready = True
        for part in self.observation:
            ready = self.report_ready and self.observation[part] is not None
        return ready

    def _set_faction(self, faction):
        if faction != BOT_CONFIG['OWN-FACTION-NAME']:
            try:
                self.observation["faction"] = Faction.objects.get(name=faction)
                self._report_ready()
                return True
            except Exception as e:
                print(e)
        else:
            # todo: log
            print("Observations cannot be created from own faction units")
            return False

    def _set_type(self, unit_type):
        if unit_type == "Infantry":
            self.observation["type"] = utypes.INFANTRY
        elif unit_type == "Mechanized":
            self.observation["type"] = utypes.MECHANIZED
        elif unit_type == "Recon":
            self.observation["type"] = utypes.RECON
        elif unit_type == "PsyOps":
            self.observation["type"] = utypes.PSYOPS
        elif unit_type == "Commander/HVT":
            self.observation["type"] = utypes.COMMANDER
        elif unit_type == "Building":
            self.observation["type"] = utypes.BUILDING
        elif unit_type == "Offgame":
            self.observation["type"] = utypes.OFFGAME
        else:
            # todo log
            print("Errorneous unit type: {}".format(unit_type))
            return False
        return True

    def _set_direction(self, direction):
        if direction == "NW":
            self.observation["direction"] = 315
        elif direction == "N":
            self.observation["direction"] = 0
        elif direction == "NE":
            self.observation["direction"] = 45
        elif direction == "E":
            self.observation["direction"] = 90
        elif direction == "SE":
            self.observation["direction"] = 135
        elif direction == "S":
            self.observation["direction"] = 180
        elif direction == "SW":
            self.observation["direction"] = 225
        elif direction == "W":
            self.observation["direction"] = 270
        else:
            # todo log
            print("Bad direction given {}".format(direction))
            return False
        return True

    def set_distance(self, distance):
        try:
            if "Over" in distance:
                self.observation["distance"] = float(distance.split(" ")[1].split("m")[0])
            else:
                self.observation["distance"] = float(distance.split("m")[0])
            return True
        except IndexError:
            # todo: log
            print("Bad distance value given")
            return False

    def set_movement(self, direction):
        if direction == "NW":
            self.observation["movement"] = 315
        elif direction == "N":
            self.observation["movement"] = 0
        elif direction == "NE":
            self.observation["movement"] = 45
        elif direction == "E":
            self.observation["movement"] = 90
        elif direction == "SE":
            self.observation["movement"] = 135
        elif direction == "S":
            self.observation["movement"] = 180
        elif direction == "SW":
            self.observation["movement"] = 225
        elif direction == "W":
            self.observation["movement"] = 270
        else:
            # todo log
            print("Bad movement direction given {}".format(direction))
            return False
        return True

    def set_size(self, size):
        if size == "1 man":
            self.observation["size"] = utypes.SINGLE
        elif size == "Patrol, 2-4 men":
            self.observation["size"] = utypes.PATROL
        elif size == "Squad, 5-12 men":
            self.observation["size"] = utypes.SQUAD
        elif size == "Platoon, 15-30 men":
            self.observation["size"] = utypes.PLATOON
        elif size == "Company, ~100 men":
            self.observation["size"] = utypes.COMPANY
        elif size == "Battalion, ~300 men":
            self.observation["size"] = utypes.BATTALION
        else:
            # todo log
            print("Bad size given {}".format(size))
            return False
        return True

    def set_vehicles(self, amount):
        self.observation["vehicles"] = amount
        return True

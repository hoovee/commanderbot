"""Place for more simple Menu definitions.

The definitions follow the Telegram API convention for defining
custom keyboards for Bot interactions.
"""

from bot.telegram.location_button import LOC_BUTTON


BOT_COMMANDS = {
    "/register": "Register your user to the system. This requires administrative approval.",
    "/start": "Start conversation with the bot if not already started.",
    "/help": "List all commands and other useful information.",
}


REGISTER_MENU = [
    ["Register"]
]


REGISTER_WAITING_MENU = [
    ["Am I in yet?"]
]


MAIN_MENU = [
    [LOC_BUTTON],
    ["Observation report"],
    ["SITREP"],
    ["Available commands"],
]

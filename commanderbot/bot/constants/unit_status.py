"""Constants for unit status."""

# For water
GREEN = 'G'
YELLOW = 'Y'
RED = 'R'

STATUS_QUANTITIES = [
    (GREEN, "Green"),
    (YELLOW, "Yellow"),
    (RED, "Red")
]

MISSION_STATUS_STARTING = 'ASM'
MISSION_STATUS_CONTINUING = 'CWM'
MISSION_STATUS_RTB = 'RTB'
MISSION_STATUS_WAITING_FOR_MISSION = 'WFM'

MISSION_STATUS = [
    (MISSION_STATUS_STARTING, 'About to start a mission'),
    (MISSION_STATUS_CONTINUING, 'Continuing with the mission'),
    (MISSION_STATUS_RTB, 'Returning to base'),
    (MISSION_STATUS_WAITING_FOR_MISSION, 'Waiting for a mission')
]

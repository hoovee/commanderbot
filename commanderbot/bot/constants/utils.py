"""Constants such as keyboards, etc."""

import logging

utils_logger = logging.getLogger('constants.utils')


def get_regex(dict, wanted_tuple, mark_between='|'):
    """Get button texts as regex 'button1text|button2text'."""
    tupl = dict[wanted_tuple]
    ls = tupl[1]
    regex_str = '^('
    for ls_item in ls:
        for item in ls_item:
            regex_str += item
            regex_str += '|'
    regex_str = regex_str[:(len(regex_str)-1)]
    regex_str += ')$'
    utils_logger.info("Final regex: {}".format(regex_str))
    return regex_str


def get_key_based_on_value(dict_to_look, value):
    for key in dict_to_look.keys():
        if dict_to_look[key] == value:
            return key
    return None

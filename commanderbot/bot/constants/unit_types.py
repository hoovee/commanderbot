"""All unit types and sizes."""

# Unit sizes
SINGLE = 'SGN'
PATROL = 'PTR'     # 2-4 men
SQUAD = 'SQD'      # 5-10 men
PLATOON = 'PLT'    # ~30 men
COMPANY = 'COY'    # ~100 men
BATTALION = 'BAT'  # ~300 men
UNIT_SIZES = [
    (SINGLE, 'One'),
    (PATROL, 'Patrol'),
    (SQUAD, 'Squad'),
    (PLATOON, 'Platoon'),
    (COMPANY, 'Company'),
    (BATTALION, 'Battalion'),
]

# Unit types
INFANTRY = 'INF'
MECHANIZED = 'MEC'
RECON = 'REC'
PSYOPS = 'PSO'
COMMANDER = 'CMD'
BUILDING = 'BLD'
CIVILIAN_PERSONNEL = 'CIP'
CIVILIAN_VEHICLE = 'CIV'
OFFGAME = 'OFF'
UNIT_TYPES = [
    (INFANTRY, 'Infantry'),
    (MECHANIZED, 'Mechanized'),
    (RECON, 'Recon'),
    (PSYOPS, 'PsyOps'),
    (COMMANDER, 'Commander'),
    (BUILDING, 'Building'),
    (CIVILIAN_PERSONNEL, 'Civilian personnel',),
    (CIVILIAN_VEHICLE, 'Civilian vehicle',),
    (OFFGAME, 'Offgame',),
]

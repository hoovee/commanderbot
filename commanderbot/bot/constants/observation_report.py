"""Observation report form and structure.

The answers to each question follow the convention how buttons are
structured in the Telegram API.
"""
from commanderbot.settings import BOT_CONFIG


OBS_REPORT = {
    1: (
        "Faction?", [
            [BOT_CONFIG['ENEMY-FACTION-NAME']],
            [BOT_CONFIG['CIVILIAN-FACTION-NAME']],
            [BOT_CONFIG['YELLOW-FACTION-NAME']],
            ["Cancel"]
        ]
    ),
    2: (
        "Type?", [
            ["Infantry", "Mechanized"],
            ["Recon", "PsyOps"],
            ["Commander/HVT", "Building"],
            ["Offgame", "Cancel"]
        ]
    ),
    3: (
        "Direction?", [
            ["NW", "N", "NE"],
            ["W", "Cancel", "E"],
            ["SW", "S", "SE"]
        ],
    ),
    4: (
        "Distance?", [
            ["25m", "50m"],
            ["100m", "150m"],
            ["200m", "250m"],
            ["300m", "400m"],
            ["500m", "Over 500m"],
            ["Cancel"],
        ],
    ),
    5: (
        "Movement?", [
            ["NW", "N", "NE"],
            ["W", "Stationary", "E"],
            ["SW", "S", "SE"],
            ["Cancel"]
        ],
    ),
    6: (
        "Size?", [
            ["1 man", "Patrol, 2-4 men"],
            ["Squad, 5-12 men", "Platoon, 15-30 men"],
            ["Company, ~100 men", "Battalion, ~300 men"],
            ["Cancel"]
        ],
    ),
    7: (
        "Number of vehicles?", [
            ["0", "1-2"],
            ["3-4", "5-6"],
            ["6+", "Cancel"]
        ],
    )
}

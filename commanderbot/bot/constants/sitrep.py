"""Situation report (SITREP) structure and questions.

Answers to each question are structured as button layouts are
given to Telegram Bot API.
"""

SITREP = {
    1: (
        "Strength?", [
            ["Green"],
            ["Yellow"],
            ["Red"],
            ["Cancel"]
        ]
    ),
    2: (
        "Ammo?", [
            ["Green"],
            ["Yellow"],
            ["Red"],
            ["Cancel"]
        ]
    ),
    3: (
        "Water?", [
            ["Green"],
            ["Yellow"],
            ["Red"],
            ["Cancel"]
        ],
    ),
    4: (
        "Energy level?", [
            ["Green"],
            ["Yellow"],
            ["Red"],
            ["Cancel"]
        ],
    ),
    5: (
        "Mission status?", [
            ["About to start a mission"],
            ["Continuing with the mission"],
            ["Returning to base"],
            ["Waiting for a mission"],
            ["Cancel"]
        ]
    )
}

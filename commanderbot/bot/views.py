"""Views for handling units."""

import datetime
from django.utils import timezone
from django.apps import apps

from bot.models import Faction, OperationalUnit, Location, Observation
from bot.serializers import (FactionSerializer, OperationalUnitSerializer,
                             LocationSerializer, ObservationSerializer)
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import filters


@api_view(['GET'])
def start_bot(request, format=None):
    apps.get_app_config('bot').bot.start_bot()
    return Response({'started': "ok"})


@api_view(['GET'])
def stop_bot(request, format=None):
    apps.get_app_config('bot').bot.stop_bot()
    return Response({'stopped': "ok"})


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'factions': reverse('factions-root', request=request, format=format),
        'operational-units': reverse('units-root', request=request, format=format),
        'start-bot': reverse('start-bot', request=request, format=format),
        'stop-bot': reverse('stop-bot', request=request, format=format),
    })


@api_view(['GET'])
def factions_root(request, format=None):
    return Response({
        'factions': reverse('faction-list', request=request, format=format),
        'observed-faction-members': '/api/factions/<name>/observed/',
    })


@api_view(['GET'])
def units_root(request, format=None):
    return Response({
        'list-units': reverse('operationalunits-list', request=request, format=format),
        'list-unapproved-units': reverse('operationalunits-unapproved', request=request, format=format),
        'list-unit-locations': '/api/units/<callsign>/locations/',
        'list-unit-observations': '/api/units/<callsign>/observations/',
    })


class FactionList(generics.ListCreateAPIView):
    """List of factions.
    """

    queryset = Faction.objects.all()
    serializer_class = FactionSerializer


class FactionDetail(generics.RetrieveUpdateDestroyAPIView):
    """Detailed information of faction.
    """

    queryset = Faction.objects.all()
    serializer_class = FactionSerializer
    lookup_field = 'name'


class FactionUnitsObserved(generics.ListAPIView):
    """Units that have been observed for this faction.
    """

    serializer_class = ObservationSerializer

    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['created', 'source']

    def get_queryset(self):
        queryset = Observation.objects.all()
        name = self.kwargs['name']
        if name is not None:
            queryset = Observation.objects.filter(faction__name=name)
        return queryset


class OperationalUnitList(generics.ListCreateAPIView):
    """Approved units.
    TODO: Make this as just list view and use different serializer
    TODO: Create separate Create view which uses the current OpUnit serializer
    List of units that have registered and approved to the system.
    """

    queryset = OperationalUnit.objects.filter(approved=True)
    serializer_class = OperationalUnitSerializer
    filter_backends = [filters.OrderingFilter]
    ordering_fields = [
        'callsign', 'type', 'updated', 'strength',
        'ammo', 'water', 'energy', 'status'
    ]


class OperationalUnitDetail(generics.RetrieveUpdateDestroyAPIView):
    """Detailed information about this operational unit.
    """

    queryset = OperationalUnit.objects.filter(approved=True)
    serializer_class = OperationalUnitSerializer
    lookup_field = 'callsign'


class UnApprovedOperationalUnits(generics.ListAPIView):
    """Unapproved units.

    List of units that have registered but are not yet approved to the system.
    """

    queryset = OperationalUnit.objects.filter(approved=False)
    serializer_class = OperationalUnitSerializer
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['callsign']


class ApproveOperationalUnit(generics.RetrieveUpdateDestroyAPIView):
    """View to update and approve certain operational unit.
    """

    queryset = OperationalUnit.objects.filter(approved=False)
    serializer_class = OperationalUnitSerializer
    lookup_field = 'callsign'


class OperationalUnitLocations(generics.ListAPIView):
    """Location history of certain operational unit.
    """

    serializer_class = LocationSerializer
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['created']

    def get_queryset(self):
        queryset = Location.objects.all()
        callsign = self.kwargs['callsign']
        if callsign is not None:
            queryset = Location.objects.filter(source__callsign=callsign)
        return queryset


class OperationalUnitObservations(generics.ListAPIView):
    """Observations made by certain operational unit.
    """

    serializer_class = ObservationSerializer
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['created', 'faction']

    def get_queryset(self):
        queryset = Observation.objects.all()
        callsign = self.kwargs['callsign']
        if callsign is not None:
            queryset = Observation.objects.filter(source__callsign=callsign)
        return queryset


class OperationalUnitObservationsFiltered(generics.ListAPIView):
    """Observations made by certain operational unit.

    Filtered so that updated value cannot be less than given minutes old.
    """

    serializer_class = ObservationSerializer
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['created', 'faction']

    def get_queryset(self):
        queryset = Observation.objects.all()
        callsign = self.kwargs['callsign']
        minutes = self.kwargs['minutes']
        if callsign and minutes:
            now = timezone.now()
            min_value = now - datetime.timedelta(minutes=minutes)
            queryset = Observation.objects.filter(source__callsign=callsign).filter(
                updated__gte=min_value
            )
        elif callsign and not minutes:
            queryset = Observation.objects.filter(source__callsign=callsign)
        return queryset


class OperationalUnitObservationsDetail(generics.RetrieveUpdateDestroyAPIView):
    """Single observation details made by certain operational unit.
    """

    serializer_class = ObservationSerializer

    def get_queryset(self):
        queryset = Observation.objects.all()
        callsign = self.kwargs['callsign']
        if callsign is not None:
            queryset = Observation.objects.filter(source__callsign=callsign)
        return queryset

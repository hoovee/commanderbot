"""Unit serializers."""

from rest_framework import serializers
from bot.models import (Faction, OperationalUnit, Observation, Location)


class FactionSerializer(serializers.ModelSerializer):
    """Serializer for Factions."""

    class Meta:
        """Meta information for our Faction serializer."""
        model = Faction
        fields = ['name', 'color', 'description']


class OperationalUnitSerializer(serializers.ModelSerializer):
    """Serializer for OperationalUnit instances."""

    partof = serializers.PrimaryKeyRelatedField(
        many=False, queryset=Faction.objects.all())

    class Meta:
        """Meta information for our Unit serializer."""
        model = OperationalUnit
        fields = [
            'callsign',
            'description',
            'size',
            'type',
            'last_position_grid',
            'last_position_e',
            'last_position_n',
            'created',
            'updated',
            'chat_id_from',
            'chat_id_to',
            'approved',
            'partof',
            'strength',
            'ammo',
            'water',
            'energy',
            'status'
        ]


class ObservationSerializer(serializers.ModelSerializer):
    """Serializer for Observation instances."""

    faction = FactionSerializer(many=False, read_only=False)
    source = OperationalUnitSerializer(many=False, read_only=False)

    class Meta:
        """Meta information for our Unit serializer."""
        model = Observation
        fields = [
            'id',
            'faction',
            'source',
            'type',
            'position_grid',
            'position_e',
            'position_n',
            'movement_direction',
            'size',
            'num_of_vehicles',
            'updated',
            'created',
            'confidence'
        ]


class LocationSerializer(serializers.ModelSerializer):
    """Serializer for Location instances."""

    source = OperationalUnitSerializer(many=False, read_only=True)

    class Meta:
        model = Location
        fields = [
            'id',
            'latitude',
            'longitude',
            'mgrs_grid',
            'mgrs_e',
            'mgrs_n',
            'created',
            'source'
        ]

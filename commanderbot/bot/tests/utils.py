def create_faction(name, color, desc):
    """Helper function for creating a faction.

    Args:
        name (string): Name of the faction.
        color (string): Color code as hex, including the # sign.
        desc (string): Detailed description of the faction.

    Returns:
        object: Object with correct fields.
    """
    return {
        'name': name,
        'color': color,
        'description': desc
    }


def create_unit(callsign, desc, size, type, partof, last_position, chat_id_from, chat_id_to):
    """Create OperationalUnit as JSON.

    Args:
        callsign (string): OperationalUnit callsign.
        desc (string): Additional details to unit.
        size (string): Select from unit_types.py. Indicates size of the unit.
        type (string): Select from unit_types.py. Indicates type of the unit.
        partof (Faction): To which faction the unit belongs to.
        last_position (tuple): Tuple with (string, int, int) values.
                               Shows MGRS grid, easting and norting.
        chat_id (int): Telegram API chat id between the bot and the user.
    """
    data = {
        'callsign': callsign,
        'description': desc,
        'size': size,
        'type': type,
        'partof': partof.id,
        'last_position_grid': last_position[0],
        'last_position_e': last_position[1],
        'last_position_n': last_position[2],
        'chat_id_from': chat_id_from,
        'chat_id_to': chat_id_to
    }
    return data

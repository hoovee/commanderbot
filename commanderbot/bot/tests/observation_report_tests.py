from django.test import TestCase

from bot.models import Faction
from bot.telegram.observations_handler import ObservationReportParser
from commanderbot.settings import BOT_CONFIG
import bot.constants.unit_types as utypes


class ObservationReportParserTests(TestCase):
    """Tests for ObservationReportParser class."""

    def setUp(self):
        self.parser = ObservationReportParser()
        Faction.objects.create(name=BOT_CONFIG['OWN-FACTION-NAME'], color='#FF0000', description='1')
        Faction.objects.create(name=BOT_CONFIG['ENEMY-FACTION-NAME'], color='#FFF000', description='2')
        Faction.objects.create(name=BOT_CONFIG['CIVILIAN-FACTION-NAME'], color='#FFFF00', description='3')
        Faction.objects.create(name=BOT_CONFIG['YELLOW-FACTION-NAME'], color='#F00000', description='4')

    def test_initialization(self):
        """Test that initialization works as intended.

        Observation and observation template should contain only None values
        and the object_ready boolean should be False.
        """
        self.parser.start_new_object()
        for key in self.parser.observation_template.keys():
            assert self.parser.observation_template[key] is None
        for key in self.parser.observation.keys():
            assert self.parser.observation[key] is None
        assert self.parser.object_ready is False

    def test_setting_enemy_faction(self):
        self.parser.start_new_object()
        assert self.parser.set_faction(BOT_CONFIG['ENEMY-FACTION-NAME']) is True
        assert self.parser.observation["faction"].name == BOT_CONFIG['ENEMY-FACTION-NAME']

    def test_setting_civilian_faction(self):
        self.parser.start_new_object()
        assert self.parser.set_faction(BOT_CONFIG['CIVILIAN-FACTION-NAME']) is True
        assert self.parser.observation["faction"].name == BOT_CONFIG['CIVILIAN-FACTION-NAME']

    def test_setting_yellow_faction(self):
        self.parser.start_new_object()
        assert self.parser.set_faction(BOT_CONFIG['YELLOW-FACTION-NAME']) is True
        assert self.parser.observation["faction"].name == BOT_CONFIG['YELLOW-FACTION-NAME']

    def test_setting_unknown_faction(self):
        self.parser.start_new_object()
        assert self.parser.set_faction(BOT_CONFIG['YELLOW-FACTION-NAME'] + "-blaa") is False
        assert self.parser.observation["faction"] is None

    def test_setting_own_faction(self):
        self.parser.start_new_object()
        assert self.parser.set_faction(BOT_CONFIG['OWN-FACTION-NAME']) is False
        assert self.parser.observation["faction"] is None

    def test_setting_existing_type(self):
        self.parser.start_new_object()
        assert self.parser.set_type("Infantry") is True
        assert self.parser.observation["type"] == utypes.INFANTRY

    def test_setting_unknown_type(self):
        self.parser.start_new_object()
        assert self.parser.set_type("Test") is False
        assert self.parser.observation["type"] is None

    def test_setting_direction(self):
        self.parser.start_new_object()
        assert self.parser.set_direction("NE") is True
        assert self.parser.observation["direction"] == 45

    def test_setting_unknown_direction(self):
        self.parser.start_new_object()
        assert self.parser.set_direction("Test") is False
        assert self.parser.observation["direction"] is None

    def test_setting_distance(self):
        self.parser.start_new_object()
        assert self.parser.set_distance("Over 500m") is True
        assert self.parser.observation["distance"] == 600
        assert self.parser.set_distance("200m") is True
        assert self.parser.observation["distance"] == 200

    def test_setting_bad_distance(self):
        self.parser.start_new_object()
        assert self.parser.set_distance("Over x") is False
        assert self.parser.set_distance("Over m") is False
        assert self.parser.set_distance("Over 4m") is False
        assert self.parser.set_distance("Over 4000m") is False

    def test_setting_movement(self):
        self.parser.start_new_object()
        assert self.parser.set_movement("Stationary") is True
        assert self.parser.observation["movement"] == -1

    def test_setting_unknown_movement(self):
        self.parser.start_new_object()
        assert self.parser.set_movement("Test") is False
        assert self.parser.observation["movement"] is None

    def test_setting_size(self):
        self.parser.start_new_object()
        assert self.parser.set_size("Squad, 5-12 men") is True
        assert self.parser.observation["size"] == utypes.SQUAD

    def test_setting_unknown_size(self):
        self.parser.start_new_object()
        assert self.parser.set_size("Test") is False
        assert self.parser.observation["size"] is None

    def test_setting_vehicles(self):
        self.parser.start_new_object()
        assert self.parser.set_vehicles("5") is True
        assert self.parser.observation["vehicles"] == 5

    def test_setting_bad_vehicle_input(self):
        self.parser.start_new_object()
        assert self.parser.set_vehicles("Test") is False
        assert self.parser.observation["vehicles"] is None

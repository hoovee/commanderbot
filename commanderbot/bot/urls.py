"""Unit app URLs."""

from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from bot import views

urlpatterns = format_suffix_patterns([
    path('bot/start/', views.start_bot, name='start-bot'),
    path('bot/stop/', views.stop_bot, name='stop-bot'),
    path('', views.api_root),
    path('factions/', views.factions_root, name='factions-root'),
    path('factions/list/', views.FactionList.as_view(), name='faction-list'),
    path('factions/<str:name>/', views.FactionDetail.as_view(), name='faction-detail'),
    path('factions/<str:name>/observed/', views.FactionUnitsObserved.as_view(), name='faction-observedunits'),
    path('units/', views.units_root, name='units-root'),
    path('units/list/', views.OperationalUnitList.as_view(), name='operationalunits-list'),
    path('units/list/unapproved/', views.UnApprovedOperationalUnits.as_view(), name='operationalunits-unapproved'),
    path('units/list/unapproved/<str:callsign>/',
         views.ApproveOperationalUnit.as_view(),
         name='operationalunits-approve'),
    path('units/<str:callsign>/', views.OperationalUnitDetail.as_view(), name='operationalunits-detail'),
    path('units/<str:callsign>/locations/',
         views.OperationalUnitLocations.as_view(),
         name='operationalunits-locations'),
    path('units/<str:callsign>/observations/',
         views.OperationalUnitObservations.as_view(),
         name='operationalunits-observations'),
    path('units/<str:callsign>/observations/<int:minutes>/',
         views.OperationalUnitObservationsFiltered.as_view(),
         name='operationalunits-observations'),
    path('units/<str:callsign>/observations/<int:pk>/',
         views.OperationalUnitObservationsDetail.as_view(),
         name='operationalunits-observations-detail'),
])

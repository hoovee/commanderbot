"""Generic utilities."""

import mgrs
import math
from bot.models import Location


class LocationManager:
    """Generic object for saving location in various coordinate systems.

    TODO: Add mgrs initialization.
    """

    def init_from_location(self, location):
        self.timestamp = location.created
        self.latitude = float(location.latitude)
        self.longitude = float(location.longitude)
        mgrs_str = self.convert_to_mgrs(self.latitude, self.longitude)
        self.mgrs_grid = mgrs_str[:5]
        self.mgrs_e = float(mgrs_str[5:10])
        self.mgrs_n = float(mgrs_str[10:])

    def init_from_lat_long_ts(self, latitude, longitude, timestamp):
        """Initialize Location object.

        Args:
            latitude (float): Latitude of the location.
            longitude (float): Longitude of the location.
            timestamp (datetime.datetime): Datetime object.
        """
        self.timestamp = timestamp
        self.latitude = latitude
        self.longitude = longitude
        mgrs_str = self.convert_to_mgrs(self.latitude, self.longitude)
        self.mgrs_grid = mgrs_str[:5]
        self.mgrs_e = float(mgrs_str[5:10])
        self.mgrs_n = float(mgrs_str[10:])

    def convert_to_mgrs(self, latitude, longitude):
        """Convert coordinate to MGRS.

        Convert coordinate to MGRS based on latitude and longitude.

        Args:
            latitude (float): latitude of the coordinate
            longitude (float): longitude of the coordinate
        Returns:
            MGRS: MGRS object
        """
        converter = mgrs.MGRS()
        mgrs_ = str(converter.toMGRS(latitude, longitude))
        return mgrs_

    def get_as_location(self, operational_unit):
        obj = Location.objects.create(
            latitude=self.latitude,
            longitude=self.longitude,
            mgrs_grid=self.mgrs_grid,
            mgrs_e=self.mgrs_e,
            mgrs_n=self.mgrs_n,
        )
        obj.source = operational_unit
        return obj

    def get_from(self, direction, distance):
        """Get MGRS coordinates from this location based on given direction and distance.

        Args:
            direction (float): Direction as degrees (0-360).
            distance (float): Distance in meters.

        Returns:
            tuple: Tuple containing mgrs grid, mgrs Easting and mgrs Northing
                   coordinate.
        """
        e_movement = math.sin(math.radians(direction)) * distance
        n_movement = math.cos(math.radians(direction)) * distance
        mgrs_e = self.mgrs_e + e_movement
        mgrs_n = self.mgrs_n + n_movement
        next_e_grid = False
        next_n_grid = False
        if mgrs_e >= 100000:
            mgrs_e -= 100000
            next_e_grid = True
        if mgrs_n >= 100000:
            mgrs_n -= 100000
            next_n_grid = True
        if next_e_grid or next_n_grid:
            # log that this is not implemented
            # todo implement grid selection based on current grid
            mgrs_grid = self.mgrs_grid
        else:
            mgrs_grid = self.mgrs_grid
        return (mgrs_grid, mgrs_e, mgrs_n)

FROM python:3.6

# do not write .pyc files
ENV PYTHONDONTWRITEBYTECODE 1
# do not buffer output to docker
ENV PYTHONUNBUFFERED 1
# set work directory which can be later used with variable $WORKDIR
WORKDIR /code
# install dependencies
RUN pip install pipenv
